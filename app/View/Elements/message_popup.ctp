<!--common popup-->
<div class="modal fade" id="msg-page" tabindex="-1" role="dialog" aria-labelledby="question-text" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header palette-market-blue">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h6 class="modal-title title-6 msg-title" id="myModalLabel"></h6>
            </div>
            <div class="modal-body msg-message"></div>
        </div>
    </div>
</div>
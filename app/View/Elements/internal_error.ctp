<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html>
<html>
<head>
    <?php echo $this->Html->charset(); ?>
    <title>
        <?php echo __('Chitchat') . ' - ' . $title_for_layout; ?>
    </title>
    <!--Mobile first-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!--IE Compatibility modes-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php
    echo $this->Html->meta('icon');
    echo $this->Html->css(array('http://fonts.googleapis.com/css?family=Roboto:400,900,700,400italic,700italic,300|Roboto+Slab:400,300', '../bootstrap/css/bootstrap'));
    ?>
    <!-- Loading Flat UI -->
    <?php echo $this->Html->css(array('flat-ui.css')); ?>
    <!-- Overrides -->
    <?php echo $this->Html->css(array('custom.css')); ?>
    <!--<link rel="shortcut icon" href="images/favicon.ico">-->
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <?php
    echo $this->fetch('meta');
    echo $this->fetch('css');
    ?>

</head>
<body class="skin-blue">

<?php
if (!isset($cur_controller)) {
    $cur_controller = $this->params['controller'];
}
if (!isset($cur_action)) {
    $cur_action = $this->params['action'];
}
?>
<div class="navbar-container palette-wet-asphalt">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-inverse navbar-default" role="navigation">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse"
                                data-target="#navbar-collapse-01"><span class="sr-only">Toggle navigation</span>
                        </button>
                        <a class="navbar-brand" href="<?php echo $baseUrl; ?>">
                            <?php echo __('Chitchat'); ?>
                        </a>
                    </div>
                    <div class="collapse navbar-collapse" id="navbar-collapse-01">

                        <div class="pull-right">
                            <?php echo $this->Html->link(__('Login'), array('controller' => 'users', 'action' => 'login'), array('class' => 'btn btn-sm navbar-btn')); ?>
                        </div>
                    </div>
                    <!-- /.navbar-collapse -->
                </nav>
                <!-- /navbar -->
            </div>
        </div>
    </div>
</div>
<div class="palette-market-blue">
    <h3 class="text-center title-content text-light serif"><?php echo __('Welcome to'); ?> <span
            class="text-inverse text-normal"><?php echo __('Chitchat'); ?></span></h3>
</div>

<div class="row">
    <div class="col-sm-8 col-sm-offset-2">
        <div class="panel panel-default panel-body text-center">
            <h1 class="text-warning"><?php echo __('Internal Error'); ?></h1>
            <!--<h4 class=""> <?php /*echo __("Internal Error!"); */?></h4>-->
            <p><?php echo __("There is some problem in our server. Please try again later."); ?></p>

        </div>
    </div>
</div>

</body>
</html>
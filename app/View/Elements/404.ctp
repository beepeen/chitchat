<div class="row">
    <div class="col-sm-8 col-sm-offset-2">
        <div class="panel panel-default panel-body text-center">
            <h1 class="text-warning"><?php echo __('404'); ?></h1>
            <h4 class=""> <?php echo __("Oops! That page can&rsquo;t be found."); ?></h4>
            <p><?php echo __("It looks like nothing was found at this location."); ?></p>
            <!--<div class="font-15"><a href="#"><?php //echo __('Home'); ?></a></div>-->
        </div>
    </div>
</div>

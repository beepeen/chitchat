<?php
/*
 * Template for Ckeditor
 */
?>
<?php
echo $this->Html->script(array('../ckeditor/ckeditor.js'), array(
    'block' => 'scriptBottom'
));
$this->start('scriptBottom');
?>

<script>
    var Chitchat = {};
    Chitchat.webroot = "<?php echo $this->webroot; ?>";
    jQuery(document).ready(function() {
        initializeBasicEditor();
    });

    function initializeBasicEditor() {
        if (jQuery('#Editor').length > 0) {
            CKEDITOR.replace('Editor',
                    {
                        toolbar: 'Chitchat',
                        toolbar_Chitchat: [
                            {name: 'basicstyles', items: ['Bold']},
                            {name: 'basicstyles', items: ['Italic']},
                            {name: 'basicstyles', items: ['Underline']},
                            {name: 'paragraph', items: ['NumberedList']},
                            {name: 'paragraph', items: ['BulletedList']},
                            {name: 'insert', items: ['Image']}
                        ],
                        height: 125,
                        autoGrow_onStartup: true,
                        //for image
                        filebrowserBrowseUrl: Chitchat.webroot + 'kcfinder/browse.php?type=files',
                        filebrowserImageBrowseUrl: Chitchat.webroot + 'kcfinder/browse.php?type=images',
                        filebrowserFlashBrowseUrl: Chitchat.webroot + 'kcfinder/browse.php?type=flash',
                        filebrowserUploadUrl: Chitchat.webroot + 'kcfinder/upload.php?type=files',
                        filebrowserImageUploadUrl: Chitchat.webroot + 'kcfinder/upload.php?type=images',
                        filebrowserFlashUploadUrl: Chitchat.webroot + 'kcfinder/upload.php?type=flash'
                    });
        }
    }

    /*
     * bootstrap modal dialog with ckeditor popup fix for input not selecting
     */
    jQuery.fn.modal.Constructor.prototype.enforceFocus = function() {
        modal_this = this;
        jQuery(document).on('focusin.modal', function(e) {
            if (modal_this.$element[0] !== e.target && !modal_this.$element.has(e.target).length
                    // add whatever conditions you need here:
                    &&
                    !$(e.target.parentNode).hasClass('cke_dialog_ui_input_select') && !$(e.target.parentNode).hasClass('cke_dialog_ui_input_text') && !$(e.target.parentNode).hasClass('cke_dialog_ui_input_textarea')) {
                modal_this.$element.focus();
            }
        });
    };
</script>
<?php $this->end(); ?>

<?php
echo $this->Session->flash();
echo $this->Session->flash('auth');
?>
<?php /* * */ ?>
<?php if (isset($errors) && !empty($errors)) { ?>
    <div class="alert alert-danger alert-dismissable box-body modalMessageElement" id="modalMessageElement">
        <!--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>-->
        <ul id="modalMessageBox" class="modalMessageBox">
            <?php foreach ($errors as $field => $error) { ?>
                <li><label for="<?php echo $field ?>" class="error"><?php echo $error[0]; ?></label></li>
            <?php } ?>
        </ul>
    </div>
    <?php
} else {
    ?>
    <div class="alert alert-danger alert-dismissable modalMessageElement" id="modalMessageElement" style="display:none">
        <!--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>-->
        <ul id="modalMessageBox" class="modalMessageBox"></ul>
    </div>
    <?php
}?>
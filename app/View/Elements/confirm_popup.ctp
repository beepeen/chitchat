<a href="#"  data-toggle="modal" data-target="#confirmPopup" class="confirm-model-link"></a>
<!-- Confirm Modal -->
<div class="modal fade" id="confirmPopup" tabindex="-1" role="dialog" aria-labelledby="question-text" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header palette-market-blue">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h6 class="modal-title title-6 confirm-title" id="myModalLabel"></h6>
            </div>
            <div class="modal-body confirm-message"></div>
            <div class="modal-footer modal-footer-sm">
                <button type="button" class="btn btn-success btn-sm confirm-yes"><?php echo __("Yes"); ?></button>
                <button type="button" class="btn btn-danger btn-sm confirm-no" data-dismiss="modal"><?php echo __("No"); ?></button>
            </div>
        </div>
    </div>
</div>

<a href="#"  data-toggle="modal" data-target="#showMessageModal" class="show-message-link"></a>
<!-- Show Message Modal -->
<div class="modal fade" id="showMessageModal" tabindex="-1" role="dialog" aria-labelledby="question-text" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header palette-market-blue">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h6 class="modal-title title-6 confirm-title" id="myModalLabel"></h6>
            </div>
            <div class="modal-body confirm-message"></div>
            <div class="modal-footer modal-footer-sm">
                <button type="button" class="btn btn-danger btn-sm confirm-no" data-dismiss="modal"><?php echo __("Ok"); ?></button>
            </div>
        </div>
    </div>
</div>
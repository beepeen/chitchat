<div class="row">
    <div class="col-md-4 col-md-offset-4 loginValidation">
        <?php echo $this->element('validate_message'); ?>
    </div>
    <div class="col-md-4 col-md-offset-4">
        <h6 class="text-center text-normal hide"><?php echo __('Existing users'); ?></h6>
        <?php echo $this->Form->create('User', array('id' => 'UserLoginForm', 'inputDefaults' => array('label' => false, 'div' => false, 'required' => false, 'error' => false))) ?>
        <div class="well well-lg">
            <div class="form-group">
                <?php echo $this->Form->input('email', array('type' => 'text', 'placeholder' => 'email', 'class' => 'form-control')); ?>
                <span class="input-icon fui-mail"></span>
            </div>
            <div class="form-group">
                <?php echo $this->Form->input('password', array('type' => 'password', 'placeholder' => 'password', 'class' => 'form-control')); ?>
                <span class="input-icon fui-lock"></span>
            </div>

            <?php echo $this->Form->submit(__('Login'), array('class' => 'btn palette-market-green btn-lg btn-block')); ?>

            <div class="form-group margin-top-10">
                <?php echo $this->Html->link(__('Sign Up'), array('controller' => 'users', 'action' => 'register'), array('class' => 'btn btn-primary btn-lg btn-block')); ?>
            </div>

            <div class="text-center spacer-top">
                <?php echo $this->Html->link(__('Lost your password?'), array('controller' => 'users','action' => 'forgot_password', 'plugin' => false), array('escape' => false)); ?>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>
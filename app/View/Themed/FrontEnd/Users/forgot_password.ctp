<div class="row">
    <div class="col-md-4 col-md-offset-4 loginValidation">
        <?php echo $this->element('validate_message'); ?>
    </div>
    <div class="col-md-4 col-md-offset-4">
        <h6 class="text-center text-normal"><?php echo __("Forgot Password"); ?></h6>

        <?php echo $this->Form->create('User', array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false, 'error' => false))) ?>
        <div class="well well-lg">
            <div class="form-group">
                <?php echo $this->Form->input('email', array('type' => 'text', 'placeholder' => 'Email', 'class' => 'form-control')); ?>
                <span class="input-icon fui-mail"></span>
            </div>
            <?php echo $this->Form->submit(__('Send me reset link'), array('class' => 'btn palette-market-green btn-lg btn-block')); ?>

            <div class="text-center spacer-top margin-top-10">
                <?php echo $this->Html->link(__('Already have a password'), array('controller' => 'users','action' => 'login', 'plugin' => false), array('escape' => false)); ?>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>

    </div>
</div>
<div class="row">
    <aside class="col-md-4 sidebar">
        <?php echo $this->element('sidebar'); ?>
    </aside>
    <main class="col-md-8">
        <?php echo $this->element("success_msg"); ?>
        <?php echo $this->element("error_msg"); ?>
        <?php echo $this->element('validate_message'); ?>
        <div class="clearfix header-main">
            <!-- user profile start -->
            <div class="col-md-12">
                <div class="box box-primary users">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo __('My Profile'); ?></h3>
                    </div><!-- /.box-header -->

                    <?php echo $this->element('validate_message'); ?>

                    <!-- form start -->
                    <?php echo $this->Form->create('User', array('id' => 'UserProfileForm', 'inputDefaults' => array('label' => false, 'div' => false, 'error' => false))); ?>
                    <div class="box-body">
                        <?php echo $this->Form->input('id', array('type' => 'hidden')); ?>
                        <?php echo $this->Form->input('group_id', array('type' => 'hidden')); ?>
                        <div class="form-group">
                            <label for="name"><?php echo __('Name') ?></label>
                            <?php
                            echo $this->Form->input('name', array(
                                'placeholder' => __('Name'),
                                'class' => 'form-control'
                            ));
                            ?>
                        </div>
                        <div class="form-group">
                            <label for="UserPhone"><?php echo __('Gender') ?></label>
                            <?php echo $this->Form->input('gender', array('options' => array(0 => __('Male'), 1 => __('Female')), 'class' => 'form-control')); ?>
                        </div>

                        <div class="form-group">
                            <label for="UserPhone"><?php echo __('Date Of Birth') ?></label>
                            <?php echo $this->Form->input('dob', array('type' => 'text', 'value' => null, 'class' => 'form-control datepicker')); ?>
                        </div>

                        <div class="form-group">
                            <label for="UserPhone"><?php echo __('Address') ?></label>
                            <?php echo $this->Form->input('User.address', array('placeholder' => __('Address'), 'class' => 'form-control', 'type'=>'textarea')); ?>
                        </div>

                        <div class="form-group">
                            <label for="name"><?php echo __('Phone') ?></label>
                            <?php
                            echo $this->Form->input('phone', array(
                                'placeholder' => __('Phone'),
                                'class' => 'form-control'
                            ));
                            ?>
                        </div>

                        <div class="form-group">
                            <label for="email"><?php echo __('Email') ?></label>
                            <?php
                            echo $this->Form->input('email', array(
                                'placeholder' => __('Email'),
                                'class' => 'form-control'
                            ));
                            ?>
                        </div>

                        <div class="form-group">
                            <label for="username"><?php echo __('Username') ?></label>
                            <?php
                            echo $this->Form->input('username', array(
                                'placeholder' => __('Username'),
                                'class' => 'form-control'
                            ));
                            ?>
                        </div>

                        <div class="form-group">
                            <label for="password"><?php echo __('Password') ?></label>
                            <?php
                            echo $this->Form->input('password', array(
                                'value' => '',
                                'class' => 'form-control',
                                'error' => false,
                                'required' => false
                            ));
                            ?>
                        </div>

                        <div class="form-group">
                            <label for="confirm_password"><?php echo __('Confirm Password') ?></label>
                            <?php
                            echo $this->Form->input('confirm_password', array(
                                'value' => '',
                                'class' => 'form-control',
                                'type' => 'password',
                                'error' => false,
                                'required' => false
                            ));
                            ?>
                        </div>
                    </div>

                    <div class="box-footer">
                        <input type="submit" value="<?php echo __('Update'); ?>" class="btn btn-primary">
                        <?php echo $this->Html->link(__('Cancel'), array('controller' => 'dashboards', 'action' => 'index'), array('class' => 'btn btn-default')); ?>
                    </div>
                    <?php echo $this->Form->end(); ?>
                    <!-- form end -->
                </div><!-- /.box -->
            </div>
            <!-- user profile end -->
        </div>
    </main>
</div>
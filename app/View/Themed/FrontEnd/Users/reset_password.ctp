<div class="row">
    <div class="col-md-4 col-md-offset-4 loginValidation">
        <?php echo $this->element('validate_message'); ?>
    </div>
    <div class="col-md-4 col-md-offset-4">
        <h6 class="text-center text-normal"><?php echo __("Reset Password"); ?></h6>

        <?php echo $this->Form->create('User', array('inputDefaults' => array('label' => false, 'div' => false, 'required' => false, 'error' => false))) ?>
        <div class="well well-lg">
            <div class="form-group">
                <?php echo $this->Form->input('name', array('type' => 'text', 'placeholder' => 'Name', 'class' => 'form-control', 'disabled' => true, 'value' => $user['User']['name'])); ?>
                <span class="input-icon fui-user"></span>
            </div>
            <div class="form-group">
                <?php echo $this->Form->input('email', array('type' => 'text', 'placeholder' => 'Email', 'class' => 'form-control', 'readonly' => 'readonly', 'value' => $user['User']['email'])); ?>
                <span class="input-icon fui-mail"></span>
            </div>
            <div class="form-group">
                <?php echo $this->Form->input('password', array('type' => 'password', 'placeholder' => 'Password', 'class' => 'form-control')); ?>
                <span class="input-icon fui-lock"></span>
            </div>
            <div class="form-group">
                <?php echo $this->Form->input('confirm_password', array('type' => 'password', 'placeholder' => 'Confirm password', 'class' => 'form-control')); ?>
                <span class="input-icon fui-lock"></span>
            </div>
            <?php echo $this->Form->submit(__('Reset Password'), array('class' => 'btn palette-market-green btn-lg btn-block')); ?>
        </div>
        <?php echo $this->Form->end(); ?>

    </div>
</div>
<div class="row">
    <aside class="col-md-4 sidebar">
        <?php echo $this->element('sidebar'); ?>
    </aside>
    <main class="col-md-8">
        <?php echo $this->element("success_msg"); ?>
        <?php echo $this->element("error_msg"); ?>
        <div class="clearfix header-main">
            <!-- user profile start -->

            <h6 class="title-5"><?php echo $user['name']; ?></h6>
            <p class="font-15">
                <?php
                    echo __('Gender: ');
                    $genderArray = array(0 => __('Male'), 1 => __('Female'));
                    echo $genderArray[$user['gender']];
                ?><br>
                <?php
                    echo __('Date of Birth: ');
                    echo (isset($user['dob']) && $user['dob']!='0000-00-00') ? $user['dob'] : 'Not available';
                ?><br>
                <?php
                    echo __('Address: ');
                    echo $user['address'];
                ?><br>
                <?php
                    echo __('Phone: ');
                    echo $user['phone'];
                ?><br>
                <?php
                    echo __('Email: ');
                    echo $user['email'];
                ?><br>
            </p>

            <!-- user profile end -->
        </div>
    </main>
</div>
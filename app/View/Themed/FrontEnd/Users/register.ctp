<div class="clearfix header-main">
    <h5><?php echo __('Register'); ?></h5>
</div>
<hr>
<div class="clearfix">
    <?php echo $this->element('validate_message'); ?>
    <?php echo $this->Form->create('User', array('id' => 'RegisterAccount', 'inputDefaults' => array('label' => false, 'div' => false, 'error' => false))); ?>
    <div class="row">
        <div class="col-md-6">
            <!--<h6 class="title-5"><?php echo __('Personal details'); ?></h6>-->
            <div class="form-group">
                <label for="UserName"><?php echo __('Name') ?></label>
                <?php echo $this->Form->input('User.name', array('placeholder' => __('Name'), 'class' => 'form-control')); ?>
            </div>
            <div class="form-group">
                <label for="UserPhone"><?php echo __('Gender') ?></label>
                <?php echo $this->Form->input('User.gender', array('options' => array(0 => __('Male'), 1 => __('Female')), 'class' => 'form-control')); ?>
            </div>

            <div class="form-group">
                <label for="UserPhone"><?php echo __('Date Of Birth') ?></label>
                <?php echo $this->Form->input('User.dob', array('type' => 'text', 'value' => null, 'class' => 'form-control datepicker')); ?>
            </div>

            <div class="form-group">
                <label for="UserPhone"><?php echo __('Address') ?></label>
                <?php echo $this->Form->input('User.address', array('placeholder' => __('Address'), 'class' => 'form-control', 'type'=>'textarea')); ?>
            </div>

            <div class="form-group">
                <label for="UserPhone"><?php echo __('Phone') ?></label>
                <?php echo $this->Form->input('User.phone', array('placeholder' => __('Phone'), 'class' => 'form-control')); ?>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="UserEmail"><?php echo __('Email') ?></label>
                <?php echo $this->Form->input('User.email', array('placeholder' => __('Email'), 'class' => 'form-control', 'error' => false)); ?>
            </div>            
            <div class="form-group">
                <label for="UserPassword"><?php echo __('Password') ?></label>
                <?php echo $this->Form->input('User.password', array('value' => '', 'class' => 'form-control', 'error' => false, 'required' => false)); ?>
            </div>
            <div class="form-group">
                <label for="UserConfirmPassword"><?php echo __('Confirm Password') ?></label>
                <?php
                echo $this->Form->input('User.confirm_password', array(
                    'value' => '',
                    'class' => 'form-control',
                    'type' => 'password',
                    'error' => false,
                    'required' => false
                ));
                ?>
            </div>
            <?php echo $this->Form->input('User.group_id', array('type' => __('hidden'), 'value' => 4)); ?>
            <div class="row">
                <div class="form-group col-md-offset-5">
                    <?php echo $this->Form->submit(__('Register'), array('class' => 'btn btn-primary btn-lg')); ?>
                </div>
            </div>
        </div>
    </div>
    <?php echo $this->Form->end(); ?>
</div>
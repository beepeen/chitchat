<?php
/*
 * Survey overview page
 */
?>
<div class="row">
    <aside class="col-md-4 sidebar">
        <?php echo $this->element('sidebar'); ?>
    </aside>
    <main class="col-md-8">
        <?php echo $this->element("success_msg"); ?>
        <?php echo $this->element("error_msg"); ?>
        <?php echo $this->element('validate_message'); ?>
        <div class="clearfix header-main">
            <div class="chatContainer">
                <h6>Welcome to the <strong>Chitchat</strong></h6>
                <p>Start by searching you friends and direct chat with them.</p>
            </div>
        </div>
    </main>
</div>

<!--confirm popup-->
<?php echo $this->element('confirm_popup'); ?>
<!--Message popup-->
<?php echo $this->element('message_popup'); ?>
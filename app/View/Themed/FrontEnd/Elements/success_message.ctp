<div class="alert alert-success alert-message">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong><?php echo __('Success!') ?></strong> <span class="message"><?php echo (isset($message)) ? $message : ''; ?></span>
</div>

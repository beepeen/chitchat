<div class="alert-fixed-cont">
    <?php
        echo $this->Session->flash();
        echo $this->Session->flash('auth', array('params' => array('class' => 'alert alert-danger alert-dismissable')));
    ?>
    <?php if (isset($errors) && !empty($errors)) { ?>
        <div class="alert alert-danger alert-dismissable" id="messageElement">
            <ul id="messageBox">
                <?php foreach ($errors as $field => $error) { ?>
                    <li><label for="<?php echo $field ?>" class="error"><?php echo $error[0]; ?></label></li>
                <?php } ?>
            </ul>
        </div>
    <?php
    } else {
        ?>
        <div class="alert alert-danger alert-dismissable" id="messageElement" style="display:none">
            <!--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>-->
            <ul id="messageBox"></ul>
        </div>
        <div class="alert alert-success alert-dismissable" id="messageElementSuccess" style="display:none">
            <!--<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>-->
            <ul id="messageBoxSuccess"></ul>
        </div>
    <?php
    }?>
</div>
<div class="modal fade" id="<?php echo $formModalId ?>" tabindex="-1" role="dialog" aria-labelledby="<?php echo $ariaLabelledby ?>" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog relative">
        <div class="loading"><div class="spinner"></div></div>
        <div class="modal-content" id="<?php echo $formModalContentId ?>">
        </div>
    </div>
</div>
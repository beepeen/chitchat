<div class="row">

    <div class="col-md-6 font-13" style="text-align:right;float:right;">
        <div class="dataTables_paginate paging_bootstrap">
            <ul class="pagination">
                <?php
                echo $this->Paginator->prev('← ' . __('previous'), array('tag' => 'li'), null, array('class' => 'prev disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                echo $this->Paginator->numbers(array('modulus' => 4, 'separator' => '', 'tag' => 'li', 'currentTag' => 'a', 'currentClass' => 'active'));
                echo $this->Paginator->next(__('next') . ' →', array('tag' => 'li'), null, array('class' => 'next disabled', 'tag' => 'li', 'disabledTag' => 'a'));
                ?>
            </ul>
        </div>
    </div>
</div>

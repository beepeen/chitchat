<!--list user contacts-->
<div class="margin-top-10">
    <?php if (!empty($contactList)) { ?>
        <table class="table table-striped table-hover font-15 table-border-top table-border-bottom">
                <?php
                foreach ($contactList as $contact) { ?>
                    <tr>
                        <td>
                            <?php echo $this->Html->link($contact['User']['name'], array('controller' => 'users', 'action' => 'view_profile', $contact['User']['id'])); ?>
                        </td>
                        <?php if(($this->params['controller'] == 'dashboards' && $this->params['action'] == 'index') || ($this->params['controller'] == 'users' && $this->params['action'] == 'ajax_search')) { ?>
                        <td align="right">
                            <?php echo $this->Html->link('<span class="glyphicon glyphicon-comment"></span>', 'javascript:void(0);', array('escape' => false, 'class' => 'chat', 'data-sender-id' => $this->Session->read('Auth.User.id'), 'data-receiver-id' => $contact['User']['id'])); ?>
                        </td>
                        <?php } ?>
                    </tr>
                <?php }; ?>
            </tbody>
        </table>
    <?php } else { ?>
        <div> <?php echo __('No users found.'); ?></div>
    <?php } ?>
</div>
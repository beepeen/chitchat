<div class="font-13">
    <h6 class="text-normal"><?php echo __('Welcome ') ?><?php echo $user['name']; ?></h6>
    <hr>

    <h6 class="text-normal"><span class="glyphicon glyphicon-search"></span> <?php echo __('Find Friends'); ?> </h6>

    <?php echo $this->Form->create('User', array('id' => 'UserSearchForm', 'url' => array('controller' => 'users', 'action' => 'ajax_search'))); ?>
        <div class="input-group">
            <?php
                echo $this->Form->input('keyword', array(
                    'type' => 'text',
                    'class' => 'form-control',
                    'placeholder' => 'Search Chitchat',
                    'label' => false,
                    'style' => 'width: 98%'
                ));
            ?>

            <div class="input-group-btn">
                <button class="btn btn-sm btn-default" type="submit"><span class="glyphicon glyphicon-search"></span></button>
            </div>
        </div>
    <?php echo $this->Form->end(); ?>
    <hr>
</div>

<div class="contactListContainer"></div>

<div class="sidebar-content">
    <ul class="nav nav-vert small" role="menu">
        <li class="nav-vert-legend"><?php echo __('Recent Users'); ?></li>
    </ul>
</div>
<?php echo $this->element('contact_list'); ?>
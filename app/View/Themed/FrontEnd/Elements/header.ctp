<?php
if (!isset($cur_controller)) {
    $cur_controller = $this->params['controller'];
}
if (!isset($cur_action)) {
    $cur_action = $this->params['action'];
}
?>
<header>
    <div class="navbar-container palette-wet-asphalt">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <nav class="navbar navbar-inverse navbar-default" role="navigation">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-01"> <span class="sr-only">Toggle navigation</span> </button>
                            <a class="navbar-brand" href="<?php echo $baseUrl; ?>">
                                <span class="text-light"><?php echo __('Chit'); ?></span><?php echo __('Chat'); ?>
                            </a>
                        </div>
                        <div class="collapse navbar-collapse" id="navbar-collapse-01">
                            <ul class="nav navbar-nav">
                                <li class="<?php echo ($cur_controller == "dashboards" && $cur_action == "index") ? 'active' : ""; ?>">
                                    <?php echo $this->Html->link(__('Home'), array('controller' => 'dashboards', 'action' => 'index')); ?>
                                </li>
                                <li class="<?php echo ($cur_controller == "users" && $cur_action == "profile") ? 'active' : ""; ?>">
                                    <?php echo $this->Html->link(__('My Profile'), array('controller' => 'users', 'action' => 'profile')); ?>
                                </li>
                            </ul>

                            <div class="pull-right">
                                <?php if ($this->Session->read('Auth.User')) { ?>
                                    <?php echo $this->Html->link(__('Log Out'), array('controller' => 'users', 'action' => 'logout'), array('class' => 'btn btn-sm navbar-btn')); ?>
                                <?php } else { ?>
                                    <?php echo $this->Html->link(__('Login'), array('controller' => 'users', 'action' => 'login'), array('class' => 'btn btn-sm navbar-btn')); ?>
                                <?php } ?>
                            </div>
                        </div>
                        <!-- /.navbar-collapse --> 
                    </nav>
                    <!-- /navbar --> 
                </div>
            </div>
        </div>
    </div>
    <div class="container">

    </div>
</header>
<?php if (!$this->Session->read('Auth.User.id')) { ?>
    <div class="palette-market-blue">
        <h3 class="text-center title-content text-light serif"><?php echo __('Welcome to'); ?> <span class="text-inverse text-normal"><?php echo __('Chitchat'); ?></span></h3>
    </div>
<?php } ?>
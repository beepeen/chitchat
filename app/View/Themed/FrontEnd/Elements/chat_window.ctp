<!--user chat window-->

<!-- form start -->
<?php echo $this->Form->create('Dashboard', array('id' => 'ChatForm', 'url' => array('controller' => 'dashboard', 'action' => 'ajax_chat'), 'inputDefaults' => array('label' => false, 'div' => false, 'error' => false))); ?>
<div class="box-body">
    <?php echo $this->Form->input('sender_id', array('type' => 'hidden')); ?>
    <?php echo $this->Form->input('receiver_id', array('type' => 'hidden')); ?>
    <div class="form-group">
        <h6><?php echo '<strong>' . $receiver['name'] . '</strong> (' . $receiver['username'] . ')'; ?></h6>
    </div>
    <div class="form-group">
        <?php
            echo $this->Form->input('textarea', array(
                'placeholder' => __('Your message here'),
                'class' => 'form-control',
                'rows' => 4
            ));
        ?>
    </div>
    <div class="pull-right">
        <button type="button" class="btn btn-primary btn-sm margin-top-5">
            <span class="glyphicon glyphicon-send"></span> <?php echo __('Send'); ?>
        </button>
    </div>
</div>

<?php echo $this->Form->end(); ?>
<!-- form end -->
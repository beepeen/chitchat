// Code that uses jQuery's can follow here.
jQuery.validator.addMethod("phoneNumber", function(phoneNumber, element) {
    phoneNumber = phoneNumber.replace(/\s+/g, "");
    return this.optional(element) || phoneNumber.length > 1 &&
        phoneNumber.match(/^[0-9-+()]+$/);
}, translate("Please enter valid phone number."));

jQuery(document).ready(function () {

    var delayTime = 5000;

    //Hide Success message  
    jQuery(".success_msg").show().delay(delayTime).fadeOut();

    jQuery('a[href=#]').on('click', function (e) {
        e.preventDefault();
    });

    jQuery(document).ajaxError(function(event, jqxhr, settings, exception) {
        if (jqxhr.status == 403) {
            msgPopUp(translate('Logout'), translate('You have been logged out from system. Please login again.'), 0);
            setTimeout(function() {
                document.location = webpath + module + "users/login"
            }, 3000);
        }
    });

    jQuery('.future').click(function () {
        alert('Oops! This functionality is coming soon.');
    });

    /*Javascript for ckeditor validation*/
    jQuery.validator.addMethod("ckeditorRequired", function (value, textarea) {
        CKEDITOR.instances[textarea.id].updateElement(); // update textarea
        var editorcontent = textarea.value.replace(/<[^>]*>/gi, ''); // strip tags

        if (editorcontent.length === 0) {
            return false;
        } else {
            return true;
        }
    }, translate('Please enter content.'));



    //Validation for User registration form
    jQuery('#UserLoginForm').validate({
        rules: {
            "data[User][email]": {
                required: true,
                email: true
            },
            "data[User][password]": "required"
        },
        messages: {
            "data[User][email]": {
                required: translate("Please enter your email."),
                email: translate("Please enter valid email.")
            },
            "data[User][password]": translate("Please enter your password.")
        },
        errorLabelContainer: "#messageBox",
        wrapper: "li",
        invalidHandler: function (event, validator) {
            jQuery("#messageElement").show().delay(delayTime).fadeOut();
        }
    });

    //Validation for Register accounts form
    jQuery('#RegisterAccount').validate({
        focusInvalid: false,
        rules: {
            "data[User][name]": "required",
            "data[User][email]": {
                required: true,
                email: true
            },
            "data[User][password]": {
                required: true,
                minlength: 6
            },
            "data[User][confirm_password]": {
                required: true,
                equalTo: "#UserPassword"
            },
            "data[User][phone]": {
                phoneNumber: true
            },
            "data[User][gender]": "required"
        },
        messages: {
            "data[User][name]": translate("Please enter name."),
            "data[User][email]": {
                required: translate("Please enter your email."),
                email: translate("Please enter valid email.")
            },
            "data[User][password]": {
                required: translate("Please enter your password."),
                minlength: translate("Please enter the 6 character password.")
            },
            "data[User][confirm_password]": {
                required: translate("Please enter the confirm password."),
                equalTo: translate("Password and Confirm password do not match.")
            },
            "data[User][phone]": {
                phoneNumber: translate("Please enter valid phone number.")
            },
            "data[User][gender]": translate("Please select your gender.")
        },
        errorLabelContainer: "#messageBox",
        wrapper: "li",
        invalidHandler: function (event, validator) {
            jQuery("#messageElement").show().delay(delayTime).fadeOut();
            jQuery('body').animate({
                scrollTop: 235
            }, 'slow');
        }
    });

    //Validation for my accounts form
    jQuery('#UserProfileForm').validate({
        focusInvalid: false,
        rules: {
            "data[User][name]": "required",
            "data[User][email]": {
                required: true,
                email: true
            },
            "data[User][password]": {
                minlength: 6
            },
            "data[User][confirm_password]": {
                equalTo: "#UserPassword"
            },
            "data[User][phone]": {
                phoneNumber: true
            },
            "data[User][gender]": "required"
        },
        messages: {
            "data[User][name]": translate("Please enter name."),
            "data[User][email]": {
                required: translate("Please enter your email."),
                email: translate("Please enter valid email.")
            },
            "data[User][password]": {
                minlength: translate("Please enter the 6 character password.")
            },
            "data[User][confirm_password]": {
                equalTo: translate("Password and Confirm password do not match.")
            },
            "data[User][phone]": {
                phoneNumber: translate("Please enter valid phone number.")
            },
            "data[User][gender]": translate("Please select your gender.")
        },
        errorLabelContainer: "#messageBox",
        wrapper: "li",
        invalidHandler: function (event, validator) {
            jQuery("#messageElement").show().delay(delayTime).fadeOut();
            jQuery('body').animate({
                scrollTop: 235
            }, 'slow');
        }
    });

    //Datepicker
    jQuery("#UserDob").datepicker({
        dateFormat: 'yy-mm-dd'
    });


    //Validation for user forgot password form
    jQuery('#UserForgotPasswordForm').validate({
        rules: {
            "data[User][email]": {
                required: true,
                email: true
            }
        },
        messages: {
            "data[User][email]": {
                required: translate("Please enter your email."),
                email: translate("Please enter valid email.")
            }
        },
        errorLabelContainer: "#messageBox",
        wrapper: "li",
        invalidHandler: function (event, validator) {
            jQuery("#messageElement").show().delay(delayTime).fadeOut();
        }
    });

    //Validation for user reset password form
    jQuery('#UserResetPasswordForm').validate({
        rules: {
            "data[User][password]": {
                required: true
            },
            "data[User][confirm_password]": {
                required: true,
                equalTo: "#UserPassword"
            }
        },
        messages: {
            "data[User][password]": translate("Please enter your password."),
            "data[User][confirm_password]": {
                required: translate("Please enter the confirm password."),
                equalTo: translate("Password and Confirm password do not match.")
            }
        },
        errorLabelContainer: "#messageBox",
        wrapper: "li",
        invalidHandler: function (event, validator) {
            jQuery("#messageElement").show().delay(delayTime).fadeOut();
        }
    });

    /* Find Users/Friends*/
    if (jQuery('#UserSearchForm').length > 0) {

        var $formObj = jQuery('#UserSearchForm');
        $formObj.ajaxForm({
            dataType: 'json',
            beforeSubmit: function (formData, jqForm, options) {
                jQuery('.loading').show();
                if(jQuery('#UserKeyword').val() == '' || jQuery('#UserKeyword').val() == null) {
                    return false;
                } else {
                    $formObj.find('button[type="submit"]').prop('disabled', true);
                }
            },
            success: function (responseText, statusText, xhr, $form) {
                jQuery('.loading').hide();
                $formObj.find('button[type="submit"]').prop('disabled', false);
                if (responseText.status == true) {
                    jQuery('.contactListContainer').html(responseText.contactListView);
                }
            }
        });
    }

    jQuery('body').on('click', '.chat', function(e) {

        if(jQuery('.chatContainer').length == 1) {
            var contact = jQuery(this);
            var senderId = contact.data('sender-id');
            var receiverId = contact.data('receiver-id');

            jQuery.ajax({
                type: "POST",
                url: webpath + "dashboards/ajax_chat_viewer",
                dataType: 'json',
                data: {
                    'senderId': senderId,
                    'receiverId': receiverId
                },
                success: function(response) {
                    if (response.status == 1) {
                        jQuery('.chatContainer').html(response.view).fadeIn('slow');
                    } else {
                        msgDisplay(response.message, true);
                    }
                },
                complete: function() {
                }
            });
        } else {
            window.location = webpath + "dashboards";
        }
    });

    // form submit prevent multiple submits
    if (jQuery.fn.safeform) {
        jQuery('form.prevent-multiple-submits').safeform();
    };
});

//php equivalent in_array function for jquery
function in_array(needle, haystack) {
    for (var i in haystack) {
        if (haystack[i] == needle)
            return true;
    }
    return false;
}

/*
function reloadMyAccount() {
    jQuery('#myAccount').html('');
    jQuery.ajax({
        type: "POST",
        url: webpath + 'companies/dashboard',
        dataType: 'html',
        success: function (response) {
            jQuery('#myAccount').html(response);
        }
    });
    jQuery('.close').click();
}*/

<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html>
<html>
    <head>
        <?php echo $this->Html->charset(); ?>
        <title>
            <?php echo __('Chitchat') . ' - ' . $title_for_layout; ?>
        </title>
        <!--Mobile first-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!--IE Compatibility modes-->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-touch-icon.png">
        <link rel="apple-touch-icon-precomposed" sizes="180x180" href="/favicons/apple-touch-icon.png">
        <link rel="icon" type="image/png" href="/favicons/favicon-32x32.png" sizes="32x32">
        <link rel="icon" type="image/png" href="/favicons/favicon-16x16.png" sizes="16x16">
        <link rel="manifest" href="/favicons/manifest.json">
        <link rel="mask-icon" href="/favicons/safari-pinned-tab.svg" color="#5bbad5">
        <link rel="shortcut icon" href="/favicons/favicon.ico">
        <meta name="msapplication-config" content="/favicons/browserconfig.xml">

        <?php
        echo $this->Html->meta('icon');
        echo $this->Html->css(array(
            //'http://fonts.googleapis.com/css?family=Roboto:400,900,700,400italic,700italic,300|Roboto+Slab:400,300',
            '../bootstrap/css/bootstrap.css'
        ));
        ?>
        <!-- Loading Flat UI -->
        <?php echo $this->Html->css(array('flat-ui.css')); ?>
        <!-- Overrides -->
        <?php echo $this->Html->css(array('custom.css', 'layout.css')); ?>

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
        <!--[if lt IE 9]>
          <script src="js/html5shiv.js"></script>
          <script src="js/respond.min.js"></script>
        <![endif]-->
        <?php
        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
        echo $this->Html->script(array('jquery.min.js'));
        ?>

        <?php
        if (isset($language) && $language != '') {
            echo $this->Html->script(array('lang.nl'), array('fullBase' => true));
        }
        ?>

        <script type="text/javascript">
            var webpath = '<?php echo $this->webroot; ?>';
            var module = '<?php echo ''; ?>'; //use empty module name to reduce routing issue for ajax calls
            var language = '<?php echo $language; ?>';
        </script>
    </head>
    <body class="skin-blue">
        <!-- Header Element -->
        <?php echo $this->element('header'); ?>
        <!-- End Header ELement -->
        <?php
        $containerClass = '';
        //$containerClass = 'full-width-container'; //full width container class
        ?>
        <div class="container content <?php echo $containerClass; ?>">
            <?php echo $this->fetch('content'); ?>
        </div>
        <!-- container -->

        <?php echo $this->element('popup_modal'); ?>
        <!-- Load JS here for greater good =============================--> 
        <?php
        $scripts = array(
            'jquery-ui-1.10.4.custom.min.js',
            'jquery.ui.touch-punch.min.js',
            'bootstrap.min.js',
            'bootstrap-select.js',
            'bootstrap-switch.js',
            'flatui-checkbox.js',
            'flatui-radio.js',
            'jquery.tagsinput.js',
            'jquery.placeholder.js',
            'jquery.validate.min.js',
            'jquery.form.min.js',
            'jquery.safeform.js',
            'jquery.cookie.js',
            'lang.nl.js',
            'main.js',
            'application.js'
        );
        echo $this->Html->script($scripts);
        ?>
        <?php echo $this->fetch('scriptBottom'); ?>
    </body>
</html>

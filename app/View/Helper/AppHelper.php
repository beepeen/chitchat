<?php

/**
 * Application level View Helper
 *
 * This file is application-wide helper file. You can put all
 * application-wide helper-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Helper
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Helper', 'View');

/**
 * Application helper
 *
 * Add your application-wide methods in the class below, your helpers
 * will inherit them.
 *
 * @package       app.View.Helper
 */
class AppHelper extends Helper {

    var $helpers = array('Session', 'Time');

    /**
     * status function to display readable status names
     * @param type $status
     * @return string 
     */
    public function status($status) {
        if ($status == "0") {
            return __("Inactive");
        } elseif ($status == "1") {
            return __("Active");
        }
    }

    /**
     * Date Format changing
     * @param type $date
     * @return type
     */
    public function dateFormat($date, $format = null) {
        if($format == null) {
            $format = "d-m-Y";
        }
        return $this->Time->format($format, $date);
    }

    /**
     * recursively unset array keys
     * @param $array
     * @param $unwantedKey
     * @return mixed
     */
    public function recursiveUnsetArrayKeys(&$array, $unwantedKey) {
        unset($array[$unwantedKey]);
        foreach ($array as &$value) {
            if (is_array($value)) {
                $this->recursiveUnsetArrayKeys($value, $unwantedKey);
            }
        }
        return $array;
    }

    /**
     * getCleanText function
     * @param $string
     * @return mixed|string
     */
    public function getCleanText($string) {
        $string = str_replace("&nbsp;", " ", $string);
        $string = str_replace("&rsquo;", "'", $string);
        $string = htmlspecialchars_decode($string, ENT_QUOTES);
        $string = trim(strip_tags($string));
        return $string;
    }

    /**
     * lowerCasedKeys for provided array
     * @param $array
     * @return array
     */
    public function lowerCasedKeys($array) {
        if(isset($array) && !empty($array)) {
            foreach($array as $key => $val) {
                $queryParam[getCleanText(strtolower($key))] = $val;
            }
            $array = $queryParam;
        }
        return $array;
    }

    /**
     * getEmailToken function
     * @param $email
     * @return string
     */
    public function getEmailToken($email) {
        $emailToken = sha1(SAMPLE_SEED . $email, false);
        return $emailToken;
    }

    /**
     * defaultDateFormat function
     * @param $date
     * @return mixed
     */
    public function defaultDateFormat($date) {
        $format = "Y-m-d";
        return $this->Time->format($format, $date);
    }
}
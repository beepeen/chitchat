<div class="row">
    <div class="col-sm-8 col-sm-offset-2">
        <div class="panel panel-default panel-body text-center">
            <h1 class="text-warning"><?php echo __('401'); ?></h1>
            <h4 class=""> <?php echo __("Your are not authorized."); ?></h4>
        </div>
    </div>
</div>

<?php

/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {

    /**
     * Get user IP address
     * @return string
     */
    public function get_ip() {

        if (getenv('HTTP_CLIENT_IP')) {
            $ip = getenv('HTTP_CLIENT_IP');
        } elseif (getenv('HTTP_X_FORWARDED_FOR')) {
            $ip = getenv('HTTP_X_FORWARDED_FOR');
        } elseif (getenv('HTTP_X_FORWARDED')) {
            $ip = getenv('HTTP_X_FORWARDED');
        } elseif (getenv('HTTP_FORWARDED_FOR')) {
            $ip = getenv('HTTP_FORWARDED_FOR');
        } elseif (getenv('HTTP_FORWARDED')) {
            $ip = getenv('HTTP_FORWARDED');
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }
    
    /**
     * Get validation error messages from all the models.
     * 
     * @param array $ignoreModels Array of model names to ignore
     * @return array Array of validation error messages
     */
    public static function getValidationErrorsAll($ignoreModels = array()) {

        $loadedModels = ClassRegistry::mapKeys();
        $validationErrors = array();

        foreach($loadedModels as $loadedModel) {
            $Model = ClassRegistry::getObject($loadedModel);

            if(!in_array($Model->name, $ignoreModels, true)) {
                if($Model->validationErrors) {
                    $validationErrors[$Model->name] = $Model->validationErrors;
                }
            }

        }

        $messages = array();
        array_walk_recursive($validationErrors, function(&$item, $key) use (&$messages, $ignoreModels) {

            if(in_array($key, $ignoreModels, true)) {
                unset($item);
                return;
            }

            if(!is_array($item)) {
                $messages[] = $item;
            }
        });
        
        $messages = array_unique($messages);
        
        return $messages;
    }

}

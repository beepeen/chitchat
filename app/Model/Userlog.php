<?php

App::uses('CleanAdminAppModel', 'CleanAdmin.Model');

/**
 * Userlog Model
 *
 * @property User $User
 */
class Userlog extends CleanAdminAppModel {

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'user_id';


    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    /**
     * 
     * @param type $user
     */
    public function createUserLog($user) {
        if (!empty($user)) {
            $this->save($user);
        }
    }

}

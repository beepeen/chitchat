<?php

App::uses('CleanAdminAppModel', 'CleanAdmin.Model');
App::uses('AuthComponent', 'Controller/Component');

/**
 * User Model
 *
 * @property User $User
 */
class User extends CleanAdminAppModel
{

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'username';

    /**
     * Behaviors
     *
     * @var array
     */
    //public $actsAs = array('Acl' => array('type' => 'requester', 'enabled' => false));

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'name' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Please enter name',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'username' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Please enter username',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            'unique' => array(
                'rule' => array('checkUniqueUsername'),
                'allowEmpty' => false,
                'message' => 'This username has already been taken.'
            ),
        ),
        'email' => array(
            'email' => array(
                'rule' => array('email'),
                'message' => 'Please enter email',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
            'unique' => array(
                'rule' => array('checkUniqueEmail'),
                'allowEmpty' => false,
                'message' => 'This email has already been taken.'
            ),
        ),
        'password' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Please enter password',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'group_id' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'confirm_password' => array(
            'match' => array(
                'rule' => array('match', 'password'),
                'allowEmpty' => false,
                'message' => 'Password and Confirm Password do not match.',
                'on' => 'reset',
            ),
        )
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed
    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Group' => array(
            'className' => 'Group',
            'foreignKey' => 'group_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    /**
     * hasOne associations
     *
     * @var array
     */
    public $hasOne = array(
        'Userlog' => array(
            'className' => 'Userlog',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    public $hasMany = array(
        'Contact' => array(
            'className' => 'Contact',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'order' => '',
            'limit' => '',
            'dependent' => true
        )
    );

    /**
     * beforeSave function for password encryption
     * @param array $options
     * @return bool
     */
    public function beforeSave($options = array()) {
        parent::beforeSave($options);
        if (isset($this->data[$this->alias]['password'])) {
            $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
        }
        return true;
    }

    /**
     * parentNode function for ACL
     * @return array|null
     */
    public function parentNode() {
        if (!$this->id && empty($this->data)) {
            return null;
        }
        if (isset($this->data['User']['group_id'])) {
            $groupId = $this->data['User']['group_id'];
        } else {
            $groupId = $this->field('group_id');
        }
        if (!$groupId) {
            return null;
        } else {
            return array('Group' => array('id' => $groupId));
        }
    }

    /**
     * bindNode function for ACL Group Binding
     * @param $user
     * @return array
     */
    public function bindNode($user) {
        //pr($user);pr(CakeSession::read());exit;
        return array('model' => 'Group', 'foreign_key' => $user['User']['group_id']);
    }

    /**
     * Identical field validation
     * @param array $field
     * @param null $compare_field
     * @return bool
     */
    public function match($field = array(), $compare_field = null) {
        foreach ($field as $key => $value) {
            $v1 = trim($value);
            $v2 = trim($this->data[$this->name][$compare_field]);
            if ($v1 !== $v2) {
                return FALSE;
            } else {
                continue;
            }
        }
        return TRUE;
    }

    /**
     * Check for uniqueness for username
     * @param $data
     * @return bool
     */
    public function checkUniqueUsername($data) {
        $isUnique = $this->find('first', array(
                'fields' => array(
                    'User.id',
                    'User.username'
                ),
                'conditions' => array(
                    'User.username' => $data['username'],
                    'User.group_id' => $this->data[$this->alias]['group_id']
                )
            )
        );

        if (!empty($isUnique)) {
            if (isset($this->id) && $this->id == $isUnique['User']['id']) {
                return true; //Allow update
            } else {
                return false; //Deny update
            }
        } else {
            return true; //If there is no match in DB allow anyone to change
        }
    }

    /**
     * Check for uniqueness for email
     * @param $data
     * @return bool
     */
    public function checkUniqueEmail($data) {
        $isUnique = $this->find('first', array(
            'fields' => array(
                'User.id',
                'User.email'
            ),
            'conditions' => array(
                'User.email' => $data['email'],
                'User.group_id' => $this->data[$this->alias]['group_id']
            )
        ));

        if (!empty($isUnique)) {
            if (isset($this->id) && $this->id == $isUnique['User']['id']) {
                return true; //Allow update
            } else {
                return false; //Deny update
            }
        } else {
            return true; //If there is no match in DB allow anyone to change
        }
    }

    /**
     * verify is the provided user is developer or not
     * @param $user
     * @return bool
     */
    public function isDeveloper($user) {
        if (isset($user) && !empty($user)) {
            if (isset($user['group_id']) && $user['group_id'] == 1) {
                return true;
            }
        }
        return false;
    }

    /**
     * getUserData function
     * @param $userId
     * @return array
     */
    public function getUserData($userId) {
        $user = $this->find('first', array(
            'conditions' => array(
                'User.id' => $userId
            )
        ));

        if(isset($user['User'])) {
            return $user['User'];
        } else {
            return null;
        }
    }

    /**
     * @param string $findType
     * @param array $options
     * @return array
     */
    public function getUsers($findType = 'all', $options = array()) {
        $users = $this->find($findType, $options);
        return $users;
    }

    public function getRecentUsers($userId) {
        $contactList = $this->find('all', array(
                'conditions' => array(
                    'User.id !=' => $userId,
                    'User.group_id' => 4
                ),
                'limit' => 10,
                'order' => array('User.id ASC')
            )
        );
        return $contactList;
    }
}

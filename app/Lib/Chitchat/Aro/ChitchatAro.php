<?php

use Chitchat\Permission\CurrentUser;

App::uses('ZoneAroInterface', 'ZoneAcl.Controller/Component/Acl');
App::uses('Group', 'Model');


class ChitchatAro implements ZoneAroInterface {
    
    const ZONE_ADMIN = 'admin';
    const ZONE_CHITCHAT = 'chitchat';

    protected $allowedZones = null;

    protected $aroData;
    
    public function setAllowedZones() {
        
        $groupId = $this->aroData['User']['group_id'];
        $this->allowedZones = array();
        
        // check if admin
        if(in_array($groupId, Group::$adminIds)) {
            $this->allowedZones[] = static::ZONE_ADMIN;
            return;
        }
        
        // for general users
        //$this->allowedZones[] = static::ZONE_CHITCHAT;
        $this->allowedZones[] = 'chitchat';
    }
    
    public function getAllowedZones($aro) {

        if($this->allowedZones === null) {
            $this->aroData = $aro;
            $this->setAllowedZones();
        }
        
        return $this->allowedZones;
    }
    
}
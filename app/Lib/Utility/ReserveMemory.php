<?php

class ReserveMemory {

    protected static $_mem = '';

    private function __construct() {

    }

    /**
     *
     * @param decimal $memory in MB
     */
    public static function reserve($memory = 1) {
        self::$_mem = str_repeat(' ', 1024 * 1024 * $memory);
    }

    public static function clear() {
        self::$_mem = '';
    }

}


<?php

App::uses('Component', 'Controller');
App::uses('Controller', 'Controller');
App::uses('ClassRegistry', 'Utility');
App::uses('AppHelper', 'View/Helper');

App::uses('MR_Jobs_CrossTab_CreateCustomVariableFile', 'Lib/MarketResponse/Jobs/CrossTab');

class CrossTabComponent extends Component {

	public function initialize(Controller $controller) {
	    $this->Controller = $controller;
	}

	public function getExternalValuesFor( $rowOptions, $model = 'CtAnswerOption' ) {

		$externalValues = array();

		if( empty($rowOptions) ) {
			return $externalValues;
		}

		$field = 'statistical_value';
		if( $model == 'AnswerOption' ) {
			$field = 'value';
		}

		$answerOptionsId = array_keys( $rowOptions );
		$answerOption = ClassRegistry::init( $model );

        if(empty($answerOptionsId)){
            return array();
        }

        if(count($answerOptionsId) == 1){
            $customCondition = array('id' => $answerOptionsId);
        } else {
            $customCondition = array('id IN' => $answerOptionsId);
        }

        $externalValues = $answerOption->find('list', array(
                'conditions' => $customCondition,
                'fields' => array('id', $field)
            )
        );

		return $externalValues;

	}

	public function getColumnOptions($colQuestionId, $answerOptionModel = 'AnswerOption') {
		$questionId = $colQuestionId;
		if( strpos($colQuestionId, '-') !== false ) {
			@list($questionId, $answerOptionsId) = explode('-', $colQuestionId);
		}

		$answerOptionObj = new $answerOptionModel;
		if( isset($answerOptionsId) && $answerOptionsId ) {
			$colOptions = $answerOptionObj->find('list', array(
					'conditions' => array(
							'question_id' => $questionId,
							'type' => 1
						)
				)
			);

		} else {
			$colOptions = $answerOptionObj->find('list', array(
					'conditions' => array(
							'question_id' => $questionId
						)
				)
			);
		}

		return array($colQuestionId => $colOptions);
	}

	public function getPercentage($total, $value) {
		if (isset($total) && isset($value)) {
			if ($total == 0) {
				return '0.0';
			} else {
				return number_format(($value / $total) * 100, 1);
			}
		}
	}

	/**
	 * process question id
	 * @param type $questionId
	 * @return type
	 */
	public function getQuestionId($questionId) {
		$tmpArray = explode('-', $questionId);
		$result = array();
		if (count($tmpArray) > 1) {
			$result['id'] = $tmpArray[0];
			$result['row_id'] = $tmpArray[1];
		} else {
			$result['id'] = $tmpArray[0];
		}
		return $result;
	}

	public function filterResponses($surveyFilterLogics, &$responses, $flag = false, $system = null) {
		if ($flag) {
			pr($surveyFilterLogics);
			pr($responses);
		}

		$responseDetailModel = 'CtResponseDetail';
		$questionModel = 'CtQuestion';
		$filterLogicModel = 'CtFilterLogic';
		if( $system == 'legacy' ) {
			$responseDetailModel = 'ResponseDetail';
			$questionModel = 'Question';
			$filterLogicModel = 'FilterLogic';
		}

		if( !empty($responses) ) {
			foreach ($responses as $responseKey => $response) {
				if (!empty($response[$responseDetailModel])) {
					//pr($response[$responseDetailModel]);
					$mainCondition = "";

					foreach ($surveyFilterLogics as $fl) {
						if ($fl != $surveyFilterLogics[0]) {
							$mainCondition.= " " . $fl[$filterLogicModel]['logic'] . " ";
						}

						$subCondition = 0;
						foreach ($response[$responseDetailModel] as $responseDetail) {
							$condition = 0;
							if ($fl[$filterLogicModel]['question_id'] == $responseDetail['question_id']) {
								if ($fl[$questionModel]['question_type_id'] == 6 || $fl[$questionModel]['question_type_id'] == 8) {
									if ($responseDetail['row_answer_option_id'] == $fl[$filterLogicModel]['row_answer_option_id']) {
										$condition = $responseDetail['column_answer_option_id'] . $fl[$filterLogicModel]['condition'] . $fl[$filterLogicModel]['column_answer_option_id'];
									}
								} else {
									$condition = $responseDetail['answer_option_id'] . $fl[$filterLogicModel]['condition'] . $fl[$filterLogicModel]['answer_option_id'];
								}

								//var_dump(eval("return ".$condition.";"));exit;

								$tmpVal = eval("return " . $condition . ";");
								if ($tmpVal) {
									$subCondition = 1;
									break;
								}
							}
						}

						$mainCondition.=$subCondition;
					}

					if (!eval("return " . $mainCondition . ";")) {
						unset($responses[$responseKey]);
					} else {

					}
				} else {
					unset($responses[$responseKey]);
				}
			}
		}
		if ($flag) {
			pr($responses);
			exit;
		}
	}

	/**
	 * get question list for survey
	 * @param type $surveyId
	 * @return string
	 */
	public function getQuestionList($surveyId, $openQuestion = false) {
		$options = array();
		if ($surveyId > 0) {
			$condition = array('question_type_id >' => 2);
			if( $openQuestion ) {
				$condition = array('question_type_id >=' => 2);
			}

			$this->Survey = ClassRegistry::init('Survey');
			$survey = $this->Survey->find('first', array(
				'conditions' => array('Survey.id' => $surveyId),
				'contain' => array(
					'SurveyPage' => array(
						'order' => 'order',
						'Question' => array(
							'AnswerOption' => array('order' => array('display_order', 'type')),
							'conditions' => $condition,
							'order' => array('display_order')
						)
					)
				)
			));

            $customTextType = $this->getCustomTextTypeFor($surveyId, 1);
			$isTextAnalysisActive = $this->isProjectAddedToTextAnalysis($surveyId, 1);
			if (!empty($survey['SurveyPage'])) {
				foreach ($survey['SurveyPage'] as $sp) {
					$pageOrder = $sp['order'];
					if (!empty($sp['Question'])) {
						foreach ($sp['Question'] as $q) {
							$questionOrder = $q['display_order'];
							$q['question'] = getCleanText($q['question']);

                            // for question type matrix and ranking
							if ($q['question_type_id'] == 6 || $q['question_type_id'] == 8) {
								if (!empty($q['AnswerOption'])) {
									$unsortedOption = array();
									foreach ($q['AnswerOption'] as $a) {
										if ($a['type'] == 0) {
											$a['option'] = $this->Survey->SurveyPage->Question->AnswerOption->getOptionText($a['option'], $a['image']);
											$unsortedOption[] = $a;
										}
									}

									if(!empty($unsortedOption)) {
										$unsortedOption = AppHelper::sortAnswerOptions($unsortedOption, $q['answer_order'], 'option'); //ordering questions answer options (natural, random or alphabetic)

										foreach ($unsortedOption as $a) {
											$optionName = $pageOrder . "." . $questionOrder . ") " . $q['question'] . " : " . $a['option'];
											$options[$q['id'] . '-' . $a['id']] = $optionName;
										}
									}
								}
							} elseif ($isTextAnalysisActive && $q['question_type_id'] == 2) {
								$optionName = $pageOrder . "." . $questionOrder . ") " . $q['question'];
								$options[$q['id']] = $optionName;
							} else {
								$optionName = $pageOrder . "." . $questionOrder . ") " . $q['question'];
								$options[$q['id']] = $optionName;
							}
						}
					}
				}
			}
		}

        //these are sample variables, that is obtained from files created
        $sampleVariableOptions = MR_Jobs_CrossTab_CreateCustomVariableFile::getCustomVariableOptions($surveyId, true);
        $options = $options + $sampleVariableOptions;

		return $options;
	}

	/**
	 * get question list for survey
	 * @param type $surveyId
	 * @return string
	 */
	public function getCtQuestionList($surveyId, $openQuestion = false) {
		$options = array();

		$this->CtSurvey = ClassRegistry::init('CtSurvey');

		if ($surveyId > 0) {
			$condition = array('question_type_id >' => 2);
			if( $openQuestion ) {
				$condition = array('question_type_id >=' => 2);
			}

			$survey = $this->CtSurvey->find('first', array(
				'conditions' => array('CtSurvey.id' => $surveyId),
				'contain' => array(
						'CtQuestion' => array(
							'CtAnswerOption',
							'conditions' => $condition,
							'order' => array('display_order')
						)
					)
				)
			);

            $customTextType = $this->getCustomTextTypeFor($surveyId, 2);
			$isTextAnalysisActive = $this->isProjectAddedToTextAnalysis($surveyId, 2);
			if (!empty($survey['CtQuestion'])) {
				foreach ($survey['CtQuestion'] as $q) {
					$questionOrder = $q['display_order'];
					$q['question'] = getCleanText($q['question']);
					if ($q['question_type_id'] == 6 || $q['question_type_id'] == 8) {
						if (!empty($q['CtAnswerOption'])) {
							foreach ($q['CtAnswerOption'] as $a) {
								$optionName = $q['external_name'] . ") " . $a['option'];
								if ($a['type'] == 0) {
									//$a['option'] = $this->Survey->SurveyPage->Question->AnswerOption->getOptionText($a['option'], $a['image']);
									$options[$q['id'] . '-' . $a['id']] = $optionName;
								}
							}
						}
					} elseif ($isTextAnalysisActive && $q['question_type_id'] == 2) {
						$optionName = $q['external_name'] . ") " . $q['question'];
						$options[$q['id']] = $optionName;
					} else {
						$optionName = $q['external_name'] . ") " . $q['question'];
						$options[$q['id']] = $optionName;
					}
				}
			}
		}

        //these are sample variables, that is obtained from files created
        $sampleVariableOptions = MR_Jobs_CrossTab_CreateCustomVariableFile::getCustomVariableOptions($surveyId, false);
        $options = $options + $sampleVariableOptions;

		return $options;
	}

	/**
	 * get answer list for survey
	 * @param type $questionId
	 * @param type $questionTypeId
	 * @return type
	 */
	public function getAnwserList($questionId, $questionTypeId) {
		$anwsers = array();
		$this->Survey = ClassRegistry::init('Survey');
		if ($questionTypeId == 2) {
			$responses = $this->Survey->SurveyPage->Question->ResponseDetail->find('all', array('conditions' => array('ResponseDetail.question_id' => $questionId, 'ResponseDetail.answer_text <>' => ''), 'fields' => array('DISTINCT ResponseDetail.answer_text')));
			foreach ($responses as $response) {
				$anwsers[$response['ResponseDetail']['answer_text']] = $response['ResponseDetail']['answer_text'];
			}
		} else if (in_array($questionTypeId, array('3', '4', '5', '7'))) {
			$responses = $this->Survey->SurveyPage->Question->AnswerOption->find('all', array('conditions' => array('AnswerOption.question_id' => $questionId), 'fields' => array('AnswerOption.id', 'AnswerOption.option', 'AnswerOption.image')));
			foreach ($responses as $response) {
				$option = $response['AnswerOption']['option'];
				if (in_array($questionTypeId, array('5', '7'))) {
					$option = $this->Survey->SurveyPage->Question->AnswerOption->getOptionText($response['AnswerOption']['option'], $response['AnswerOption']['image']);
				}
				$anwsers[$response['AnswerOption']['id']] = $option;
			}
		} else if (in_array($questionTypeId, array('6', '8'))) {
			$responses = $this->Survey->SurveyPage->Question->AnswerOption->find('all', array('conditions' => array('AnswerOption.question_id' => $questionId, 'AnswerOption.type' => 1), 'fields' => array('AnswerOption.id', 'AnswerOption.option', 'AnswerOption.image')));
			foreach ($responses as $response) {
				$anwsers[$response['AnswerOption']['id']] = $this->Survey->SurveyPage->Question->AnswerOption->getOptionText($response['AnswerOption']['option'], $response['AnswerOption']['image']);
			}
		}
		return $anwsers;
	}

	/**
	 * get answer list for survey
	 * @param type $questionId
	 * @param type $questionTypeId
	 * @return type
	 */
	public function getCtAnwserList($questionId, $questionTypeId) {
		$answers = array();

		$this->CtResponseDetail = ClassRegistry::init('CtResponseDetail');
		$this->CtAnswerOption = ClassRegistry::init('CtAnswerOption');
		if ($questionTypeId == 2) {
			$responses = $this->CtResponseDetail->find('all', array('conditions' => array('CtResponseDetail.question_id' => $questionId, 'CtResponseDetail.answer_text <>' => ''), 'fields' => array('DISTINCT CtResponseDetail.answer_text')));
			foreach ($responses as $response) {
				$answers[$response['CtResponseDetail']['answer_text']] = $response['CtResponseDetail']['answer_text'];
			}
		} else if (in_array($questionTypeId, array('3', '4', '5', '7'))) {
			$responses = $this->CtAnswerOption->find('all', array('conditions' => array('CtAnswerOption.question_id' => $questionId), 'fields' => array('CtAnswerOption.id', 'CtAnswerOption.option')));
			foreach ($responses as $response) {
				$option = $response['CtAnswerOption']['option'];
				$answers[$response['CtAnswerOption']['id']] = $option;
			}
		} else if (in_array($questionTypeId, array('6'))) {
			$responses = $this->CtAnswerOption->find('all', array('conditions' => array('CtAnswerOption.question_id' => $questionId, 'CtAnswerOption.type' => 1), 'fields' => array('CtAnswerOption.id', 'CtAnswerOption.option')));
			foreach ($responses as $response) {
				$answers[$response['CtAnswerOption']['id']] = $response['CtAnswerOption']['option'];
			}
		}

		return $answers;
	}

	/**
	 * get previous filters for survey
	 * @param type $filterId
	 * @param type $surveyId
	 * @return string
	 */
	public function getPreviousFilterHtml($filterId = null, $surveyId = null) {
		$prevFilterData = array();
		$this->Survey = ClassRegistry::init('Survey');
		if (!empty($filterId) && $filterId > 0) {
			$filters = $this->Survey->Filter->find('first', array('conditions' => array('Filter.id' => $filterId), 'contain' => array('FilterLogic' => array('SampleFilterLogic'))));
		}

		if (!empty($filters)) {
			$this->Controller->request->data['Filter'] = $filters['Filter'];
			$i = 0;
			foreach ($filters['FilterLogic'] as $filterLogic) {
                if(($filterLogic['question_id'] == 0) && !empty($filterLogic['SampleFilterLogic'])){
                    $question['Question'] = array(
                        'id' => 0,
                        'name' => str_replace('sample-', '', $filterLogic['SampleFilterLogic']['question']),
                        'question_type_id' => 'sample',
                        'question' => $filterLogic['SampleFilterLogic']['question']
                    );
                    $question['AnswerOption'] = array();
                } else {
                    $question = $this->Survey->SurveyPage->Question->find('first', array('conditions' => array('Question.id' => $filterLogic['question_id']), 'contain' => array('AnswerOption')));
                    $filterLogic['SampleFilterLogic'] =  array();
                }

				if (!empty($question)) {
					$questionId = $question['Question']['id'];
                    if($question['Question']['question_type_id'] == 'sample'){
                        $answerList = MR_Jobs_CrossTab_CreateCustomVariableFile::getSampleAnswerOptions($filters['Filter']['survey_id'], $question['Question']['name'], 'legacy');
                    } else {
                        $answerList = $this->getAnwserList($questionId, $question['Question']['question_type_id']);
                    }
					$view = new View();
					if ($question['Question']['question_type_id'] == 6 || $question['Question']['question_type_id'] == 8) {
						$prevFilterData[$i]['question_id'] = $questionId . '-' . $filterLogic['row_answer_option_id'];
					} elseif($question['Question']['question_type_id'] == 'sample') {
                        $prevFilterData[$i]['question_id'] = $question['Question']['question'];
                    } else {
						$prevFilterData[$i]['question_id'] = $questionId;
					}

					$prevFilterData[$i]['logic'] = $filterLogic['logic'];
					$prevFilterData[$i]['filter_logic_id'] = $filterLogic['id'];
					$prevFilterData[$i]['condition_html'] = $view->element('../Themed/FrontEnd/CrossTabs/get_answer_condition', array('question' => $question, 'selectCondition' => $filterLogic['condition']));
					$selectAnwser = "";
					if ($question['Question']['question_type_id'] == 2) {
						$selectAnwser = $filterLogic['answer_text'];
					} elseif ($question['Question']['question_type_id'] == 6 || $question['Question']['question_type_id'] == 8) {
						$selectAnwser = $filterLogic['column_answer_option_id'];
					} elseif($question['Question']['question_type_id'] == 'sample') {
                        $selectAnwser = $filterLogic['SampleFilterLogic']['answer'];
                    } else {
						$selectAnwser = $filterLogic['answer_option_id'];
					}
					$prevFilterData[$i]['answers_html'] = $view->element('../Themed/FrontEnd/CrossTabs/get_answer_list', array('question' => $question, 'answerList' => $answerList, 'selectAnwser' => $selectAnwser));
					$i++;
				}
			}
		}

		if (!empty($prevFilterData)) {
			$questions = $this->getQuestionList($surveyId, true);
			$previousFilterHtml = $view->element('../Themed/FrontEnd/CrossTabs/previous_filters', array('prevFilterData' => $prevFilterData, 'questions' => $questions));
		} else {
			$previousFilterHtml = "";
		}
		return $previousFilterHtml;
	}

	/**
	 * get previous filters for survey
	 * @param type $filterId
	 * @param type $surveyId
	 * @return string
	 */
	public function getCtPreviousFilterHtml($filterId = null, $surveyId = null) {

		$this->CtFilter = ClassRegistry::init('CtFilter');
		$this->CtQuestion = ClassRegistry::init('CtQuestion');

		$view = new View();
		$prevFilterData = array();
		if (!empty($filterId) && $filterId > 0) {
			$filters = $this->CtFilter->find('first', array('conditions' => array('CtFilter.id' => $filterId), 'contain' => array('CtFilterLogic' => array('SampleFilterLogic'))));
		}

		if (!empty($filters)) {
			$this->Controller->request->data['CtFilter'] = $filters['CtFilter'];
			$i = 0;
			foreach ($filters['CtFilterLogic'] as $filterLogic) {
                if(($filterLogic['question_id'] == 0) && !empty($filterLogic['SampleFilterLogic'])){
                    $question['CtQuestion'] = array(
                        'id' => 0,
                        'name' => str_replace('sample-', '', $filterLogic['SampleFilterLogic']['question']),
                        'question_type_id' => 'sample',
                        'question' => $filterLogic['SampleFilterLogic']['question']
                    );
                    $question['CtAnswerOption'] = array();
                } else {
                    $question = $this->CtQuestion->find('first', array('conditions' => array('CtQuestion.id' => $filterLogic['question_id']), 'contain' => array('CtAnswerOption')));
                    $filterLogic['SampleFilterLogic'] =  array();
                }

                if (!empty($question)) {
					$questionId = $question['CtQuestion']['id'];
                    if($question['CtQuestion']['question_type_id'] == 'sample'){
                        $answerList = MR_Jobs_CrossTab_CreateCustomVariableFile::getSampleAnswerOptions($filters['CtFilter']['survey_id'], $question['CtQuestion']['name'], '');
                    } else {
                        $answerList = $this->getCtAnwserList($questionId, $question['CtQuestion']['question_type_id']);
                    }

					if ($question['CtQuestion']['question_type_id'] == 6 || $question['CtQuestion']['question_type_id'] == 8) {
						$prevFilterData[$i]['question_id'] = $questionId . '-' . $filterLogic['row_answer_option_id'];
					} elseif($question['CtQuestion']['question_type_id'] == 'sample') {
                        $prevFilterData[$i]['question_id'] = $question['CtQuestion']['question'];
                    } else {
						$prevFilterData[$i]['question_id'] = $questionId;
					}

					$prevFilterData[$i]['logic'] = $filterLogic['logic'];
					$prevFilterData[$i]['filter_logic_id'] = $filterLogic['id'];
					$prevFilterData[$i]['condition_html'] = $view->element('../Themed/FrontEnd/CrossTabs/get_ct_answer_condition', array('question' => $question, 'selectCondition' => $filterLogic['condition']));
					$selectAnwser = "";
					if ($question['CtQuestion']['question_type_id'] == 2) {
						$selectAnwser = $filterLogic['answer_text'];
					} elseif ($question['CtQuestion']['question_type_id'] == 6 || $question['CtQuestion']['question_type_id'] == 8) {
						$selectAnwser = $filterLogic['column_answer_option_id'];
					} elseif($question['CtQuestion']['question_type_id'] == 'sample') {
                        $selectAnwser = $filterLogic['SampleFilterLogic']['answer'];
                    } else {
						$selectAnwser = $filterLogic['answer_option_id'];
					}
					$prevFilterData[$i]['answers_html'] = $view->element('../Themed/FrontEnd/CrossTabs/get_answer_list', array('question' => $question, 'answerList' => $answerList, 'selectAnwser' => $selectAnwser));
					$i++;
				}
			}
		}

		if (!empty($prevFilterData)) {
			$questions = $this->getCtQuestionList($surveyId, true);
			$previousFilterHtml = $view->element('../Themed/FrontEnd/CrossTabs/previous_filters', array('prevFilterData' => $prevFilterData, 'questions' => $questions));
		} else {
			$previousFilterHtml = "";
		}

		return $previousFilterHtml;
	}

	public function getFrequencyOptimized($surveyId, $rowOptions, $columnOptions, $responses, $responseOptionTotalFrequency, $responseOptionTotalColumnFrequency, $crossTabRowQuestionCount, $system = '') {

		$responseFrequency = array();
		$responseInitialRowTotal = array();
		$rowResponseFrequency = array();
		$rowQuestionResponseCounter = 0;

		if (isset($responses) && !empty($responses)) {

			$responseInitialRowTotal['total'] = $rowQuestionResponseCounter;


			foreach ($rowOptions as $rowQuestionKey => $rowOption) {

				//check for row matrix options
				if (strpos($rowQuestionKey, '-') !== false) {
					$questionId = reset(explode('-', $rowQuestionKey));
					$rowQuestionId = end(explode('-', $rowQuestionKey));
				} else {
					$questionId = $rowQuestionKey;
					$rowQuestionId = 0;
				}

				//get total respondent of each question
                $rowQuestionResponseCounter = isset($crossTabRowQuestionCount[$rowQuestionKey]['count']) ? $crossTabRowQuestionCount[$rowQuestionKey]['count'] : 0;
				$responseInitialRowTotal['total'] = $rowQuestionResponseCounter;
				$responseInitialRowTotal[$rowQuestionKey]['total'] = $rowQuestionResponseCounter;

				foreach ($rowOption as $rowOptionKey => $rowOpt) {

					if (isset($rowQuestionId) && $rowQuestionId != 0) {
						$responseRowOptionKey = $rowQuestionId . '-' . $rowOptionKey;
					} else {
						$responseRowOptionKey = $rowOptionKey;
					}

					//case when columnOptions is selected
					if (isset($columnOptions) && !empty($columnOptions)) {
						foreach ($columnOptions as $columnQuestionKey => $columnOption) {
							if( $system == 'legacy' ) {
								$externalValues = $this->getExternalValuesFor( $columnOption, 'AnswerOption' );
							} else {
								$externalValues = $this->getExternalValuesFor( $columnOption );
							}
							//check for row matrix options
							if (strpos($columnQuestionKey, '-') !== false) {
								$colQuestionId = end(explode('-', $columnQuestionKey));
							} else {
								$colQuestionId = 0;
							}
							$responseFrequency[$rowQuestionKey][$columnQuestionKey][$rowOptionKey]['f'] = 0;
							$responseFrequency[$rowQuestionKey][$columnQuestionKey][$rowOptionKey]['fx'] = 0;
							$responseFrequency[$rowQuestionKey][$columnQuestionKey][$rowOptionKey]['fx2'] = 0;
							foreach ($columnOption as $columnOptionKey => $colOpt) {
								if (isset($colQuestionId) && $colQuestionId != 0) {
									$responseColumnOptionKey = $colQuestionId . '-' . $columnOptionKey;
								} else {
									$responseColumnOptionKey = $columnOptionKey;
								}

								if (isset($responses[$responseRowOptionKey][$responseColumnOptionKey]['count']) && !empty($responses[$responseRowOptionKey][$responseColumnOptionKey]['count'])) {
									$responseFrequency[$rowQuestionKey][$columnQuestionKey][$rowOptionKey][$columnOptionKey] = $responses[$responseRowOptionKey][$responseColumnOptionKey]['count'];
									if( isset( $externalValues[$columnOptionKey]) ) {
										if( isset($externalValues[$columnOptionKey]) && (!is_null($externalValues[$columnOptionKey]) || $externalValues[$columnOptionKey] != '') ) {
											$exValue = $externalValues[$columnOptionKey];
											$exValue = str_replace(',', '.', $exValue);
											$responseFrequency[$rowQuestionKey][$columnQuestionKey][$rowOptionKey]['f'] += $responses[$responseRowOptionKey][$responseColumnOptionKey]['count'];
											$responseFrequency[$rowQuestionKey][$columnQuestionKey][$rowOptionKey]['fx'] += $exValue * $responses[$responseRowOptionKey][$responseColumnOptionKey]['count'];
											$responseFrequency[$rowQuestionKey][$columnQuestionKey][$rowOptionKey]['fx2'] += $exValue * $exValue * $responses[$responseRowOptionKey][$responseColumnOptionKey]['count'];
										}
									}
								} else {
									$responseFrequency[$rowQuestionKey][$columnQuestionKey][$rowOptionKey][$columnOptionKey] = 0;
								}

								if (isset($responseOptionTotalColumnFrequency[$rowQuestionKey][$responseColumnOptionKey]['count']) && !empty($responseOptionTotalColumnFrequency[$rowQuestionKey][$responseColumnOptionKey]['count'])) {
									$responseInitialRowTotal[$rowQuestionKey][$columnQuestionKey][$columnOptionKey] = $responseOptionTotalColumnFrequency[$rowQuestionKey][$responseColumnOptionKey]['count'];
								} else {
									$responseInitialRowTotal[$rowQuestionKey][$columnQuestionKey][$columnOptionKey] = 0;
								}
								if (!isset($rowResponseFrequency[$rowQuestionKey]['total']) || empty($rowResponseFrequency[$rowQuestionKey]['total'])) {
									$rowResponseFrequency[$rowQuestionKey]['total'] = 0;
									$rowResponseFrequency[$rowQuestionKey]['fx'] = 0;
									$rowResponseFrequency[$rowQuestionKey]['fx2'] = 0;
								}

								if (!isset($rowResponseFrequency[$rowQuestionKey][$rowOptionKey]) || empty($rowResponseFrequency[$rowQuestionKey][$rowOptionKey])) {
									$rowResponseFrequency[$rowQuestionKey][$rowOptionKey] = $responseOptionTotalFrequency[$responseRowOptionKey]['count'];
									$rowResponseFrequency[$rowQuestionKey]['total'] += $rowResponseFrequency[$rowQuestionKey][$rowOptionKey];
									if( isset( $externalValues[$columnOptionKey]) && (!is_null($externalValues[$columnOptionKey]) || $externalValues[$columnOptionKey] != '') ) {
										$rowResponseFrequency[$rowQuestionKey]['fx'] += $externalValues[$columnOptionKey] * $rowResponseFrequency[$rowQuestionKey][$rowOptionKey];
										$rowResponseFrequency[$rowQuestionKey]['fx2'] += $externalValues[$columnOptionKey] * $externalValues[$columnOptionKey] * $rowResponseFrequency[$rowQuestionKey][$rowOptionKey];
									}
								}
							}

							unset($colQuestionId);
						}
					} else {
						if( $system == 'legacy' ) {
							$externalValues = $this->getExternalValuesFor( $rowOption, 'AnswerOption' );
						} else {
							$externalValues = $this->getExternalValuesFor( $rowOption );
						}
                        if (isset($responseOptionTotalFrequency[$responseRowOptionKey]['count']) && !empty($responseOptionTotalFrequency[$responseRowOptionKey]['count'])) {
							$responseFrequency[$rowQuestionKey][$rowOptionKey] = $responseOptionTotalFrequency[$responseRowOptionKey]['count'];
						} else {
							$responseFrequency[$rowQuestionKey][$rowOptionKey] = 0;
						}

						if (isset($responseOptionTotalColumnFrequency[$rowQuestionKey][$responseRowOptionKey]['count']) && !empty($responseOptionTotalColumnFrequency[$rowQuestionKey][$responseRowOptionKey]['count'])) {
							$responseInitialRowTotal[$rowQuestionKey][$rowOptionKey] = $responseOptionTotalColumnFrequency[$rowQuestionKey][$responseRowOptionKey]['count'];
						} else {
							$responseInitialRowTotal[$rowQuestionKey][$rowOptionKey] = 0;
						}
						if (!isset($rowResponseFrequency[$rowQuestionKey]['total']) || empty($rowResponseFrequency[$rowQuestionKey]['total'])) {
							$rowResponseFrequency[$rowQuestionKey]['total'] = 0;
							$rowResponseFrequency[$rowQuestionKey]['fx'] = 0;
							$rowResponseFrequency[$rowQuestionKey]['fx2'] = 0;
						}

						if (!isset($rowResponseFrequency[$rowQuestionKey][$rowOptionKey]) || empty($rowResponseFrequency[$rowQuestionKey][$rowOptionKey])) {
							$rowResponseFrequency[$rowQuestionKey][$rowOptionKey] = $responseOptionTotalFrequency[$responseRowOptionKey]['count'];
							$rowResponseFrequency[$rowQuestionKey]['total'] += $rowResponseFrequency[$rowQuestionKey][$rowOptionKey];
							if( isset($externalValues[$rowOptionKey]) ) {
								$rowResponseFrequency[$rowQuestionKey]['fx'] += $externalValues[$rowOptionKey] * $rowResponseFrequency[$rowQuestionKey][$rowOptionKey];
								$rowResponseFrequency[$rowQuestionKey]['fx2'] += $externalValues[$rowOptionKey] * $externalValues[$rowOptionKey] * $rowResponseFrequency[$rowQuestionKey][$rowOptionKey];
							}
						}
					}
				}
				unset($rowQuestionId);
			}
			$optimizedResponse['responseInitialRowTotal'] = $responseInitialRowTotal;
			$optimizedResponse['rowResponseFrequency'] = $rowResponseFrequency;
			$optimizedResponse['responseFrequency'] = $responseFrequency;
		} else {
			$optimizedResponse['responseInitialRowTotal'] = null;
			$optimizedResponse['rowResponseFrequency'] = null;
			$optimizedResponse['responseFrequency'] = null;
		}

		return $optimizedResponse;
	}

	/**
	 * @param $surveyId
	 * @param string $system
	 * @return mixed
     */
	public function getTextAnalysisCategoryData($surveyId, $system = '') {
		$surveyType = 2; //imported
		if ($system == 'legacy') {
			$surveyType = 1; //native
		}

		$this->TextAnalysis = ClassRegistry::init('TextAnalysis.TextAnalysis');
		$this->TextAnalysis->Behaviors->attach('Containable');
		$taCategories = $this->TextAnalysis->find('first', array(
				'conditions' => array(
						'survey_id' => $surveyId,
						'survey_type' => $surveyType
					),
				'contain' => array('TaSubCategoryType')
			)
		);

		return $taCategories;
	}

	/**
	 * @param $surveyId
	 * @param string $system
	 * @return array
	 */
	public function getFirstLevelCategories($surveyId, $system = '') {
		$surveyType = 2; //imported
		if ($system == 'legacy') {
			$surveyType = 1; //native
		}

		$this->TaResponseTextType = ClassRegistry::init('TextAnalysis.TaResponseTextType');
		$taFirstLevelCategories = $this->TaResponseTextType->find('list', array(
				'conditions' => array(
					'is_custom_type' => 0,
				),
				'fields' => array('id', 'text_type')
			)
		);

		$taFirstLevelCustomCategories = $this->getCustomCategory($surveyId, $surveyType);

        $taFirstLevelCustomCategoryIds = Set::classicExtract($taFirstLevelCustomCategories, 'TaCustomTextType.{n}.TaResponseTextType.id');
        $taFirstLevelCustomCategories = Set::classicExtract($taFirstLevelCustomCategories, 'TaCustomTextType.{n}.TaResponseTextType.text_type');

        $customCategories = array_combine($taFirstLevelCustomCategoryIds, $taFirstLevelCustomCategories);

		return $taFirstLevelCategories + $customCategories;
	}

    public function getCustomCategory($surveyId, $surveyType)
    {
        $this->TextAnalysis = ClassRegistry::init('TextAnalysis.TextAnalysis');
        $this->TextAnalysis->Behaviors->attach('Containable');

        return $this->TextAnalysis->find('first', array(
            'conditions' => array('survey_id' => $surveyId, 'survey_type' => $surveyType),
            'contain' => array('TaCustomTextType.TaResponseTextType')
        ));
    }

	public function getCodedOpenAnswers($surveyId, $system = '') {
        $surveyType = 2; //imported
        if ($system == 'legacy') {
            $surveyType = 1; //native
        }

        $this->TextAnalysis = ClassRegistry::init('TextAnalysis.TextAnalysis');
		$this->TextAnalysis->Behaviors->attach('containable');

        $options['conditions'] = array('survey_id' => $surveyId, 'survey_type' => $surveyType);
        $options['contain'] = array('TaResponseTextMeta');
        $taCodedOpenAnswers = $this->TextAnalysis->find('first', $options);

        if (!empty($taCodedOpenAnswers['TaResponseTextMeta'])) {
            $taCoded = Set::classicExtract($taCodedOpenAnswers, 'TaResponseTextMeta.{n}.response_detail_id');
            $taCodedMeta = Set::classicExtract($taCodedOpenAnswers, 'TaResponseTextMeta.{n}.meta_text');

            return array_combine($taCoded, $taCodedMeta);
        }

        return $taCodedOpenAnswers;
    }

    //added by Soviet, for getting coded open answers for the given question id
    public function getCodedOpenAnswersByQuestionId($surveyId = null, $system = '', $questionId) {
    	//Note: we dont need surveyId and $system (surveyType) for this.
        $this->TextAnalysis = ClassRegistry::init('TextAnalysis.TextAnalysis');
		//$this->TextAnalysis->Behaviors->attach('containable');

		$taCodedOpenAnswers = $this->TextAnalysis->TaResponseTextMeta->find('list', array(
													'conditions' => array(
														'TaResponseTextMeta.question_id' => $questionId
														),
													'fields' => array('TaResponseTextMeta.response_detail_id','TaResponseTextMeta.meta_text')
													));

        
    	//debug($taCodedOpenAnswers);
        return count($taCodedOpenAnswers) > 0 ? $taCodedOpenAnswers : false;
    }

	/**
	 * Check if the survey project is added for textanalysis
	 *
	 * @param null $surveyId
	 * @param null $surveyType
	 * @return bool
     */
	public function isProjectAddedToTextAnalysis($surveyId = null, $surveyType = null) {
		$this->TextAnalysis = ClassRegistry::init('TextAnalysis.TextAnalysis');
		$this->TextAnalysis->useTable = 'ta_text_analyses';
		$surveyCount = $this->TextAnalysis->find('count', array(
				'conditions' => array(
					'TextAnalysis.survey_id' => $surveyId,
					'TextAnalysis.survey_type' => $surveyType
				)
			)
		);

		return $surveyCount > 0 ? true : false;
	}

    public function getCustomTextTypeFor($surveyId, $surveyType) {
        $this->TextAnalysis = ClassRegistry::init('TextAnalysis.TextAnalysis');
        $this->TextAnalysis->Behaviors->attach('Containable');
        $this->TextAnalysis->useTable = 'ta_text_analyses';
        $customTextType = $this->TextAnalysis->find('first', array(
                'conditions' => array(
                    'TextAnalysis.survey_id' => $surveyId,
                    'TextAnalysis.survey_type' => $surveyType
                ),
                'contain' => array(
                    'TaCustomTextType.TaResponseTextType'
                ),
            )
        );

        if (empty($customTextType['TaCustomTextType'])) {
            return array();
        }

        $id = Set::classicExtract($customTextType, 'TaCustomTextType.{n}.TaResponseTextType.id');
        $textType = Set::classicExtract($customTextType, 'TaCustomTextType.{n}.TaResponseTextType.text_type');

        return $arr[$id[0]] = $textType[0];
    }


    /**
     * Function to get frequency optimized for sample variable(and for non sample variable too)
     * @param $surveyId
     * @param $rowOptions
     * @param $columnOptions
     * @param $responses
     * @param $responseOptionTotalFrequency
     * @param $responseOptionTotalColumnFrequency
     * @param $crossTabRowQuestionCount
     * @param string $system
     * @param bool $rowColSampleFileData
     * @return mixed
     */
    public function getFrequencyOptimizedForSampleVariable($surveyId, $rowOptions, $columnOptions, $responses, $responseOptionTotalFrequency, $responseOptionTotalColumnFrequency, $crossTabRowQuestionCount, $system = '', $rowColSampleFileData = false) {

        $responseFrequency = array();
        $responseInitialRowTotal = array();
        $rowResponseFrequency = array();
        $rowQuestionResponseCounter = 0;
        if (isset($responses) && !empty($responses)) {

            $responseInitialRowTotal['total'] = $rowQuestionResponseCounter;
            foreach ($rowOptions as $rowQuestionKey => $rowOption) {
                if(strpos($rowQuestionKey, 'sample-') !== false){
                    $questionId = 'sample';
                    $rowQuestionId = $rowQuestionKey;
                    //$rowQuestionId = str_replace('sample-','', $rowQuestionKey);
                } else if (strpos($rowQuestionKey, '-') !== false) {
                    //check for row matrix options
                    $partsRowQuestionKey = explode('-', $rowQuestionKey);
                    $questionId = reset($partsRowQuestionKey);
                    $rowQuestionId = end($partsRowQuestionKey);
                } else {
                    $questionId = $rowQuestionKey;
                    $rowQuestionId = 0;
                }

                //get total respondent of each question
                $rowQuestionResponseCounter = isset($crossTabRowQuestionCount[$rowQuestionKey]['count']) ? $crossTabRowQuestionCount[$rowQuestionKey]['count'] : 0;
                $responseInitialRowTotal['total'] = $rowQuestionResponseCounter;
                $responseInitialRowTotal[$rowQuestionKey]['total'] = $rowQuestionResponseCounter;

                /*
                 * added for summary table to distinguish total based on row option question
                 * @author: BeepeEn ManandHar
                 */
                $responseInitialRowTotal[$questionId]['total'] = $rowQuestionResponseCounter;

                foreach ($rowOption as $rowOptionKey => $rowOpt) {
                    if($questionId == 'sample'){
                        $responseRowOptionKey = 'sample-'.$rowOptionKey;
                    } else if (isset($rowQuestionId) && $rowQuestionId != 0) {
                        $responseRowOptionKey = $rowQuestionId . '-' . $rowOptionKey;
                    } else {
                        $responseRowOptionKey = $rowOptionKey;
                    }
                    //case when columnOptions is selected
                    if (isset($columnOptions) && !empty($columnOptions)) {
                        foreach ($columnOptions as $columnQuestionKey => $columnOption) {
                            if( $system == 'legacy' ) {
                                $newColumnOption = $columnOption;
                                if(strpos($columnQuestionKey, 'sample-') !== false){
                                    $newColumnOption = array();
                                }
                                $externalValues = $this->getExternalValuesFor( $newColumnOption, 'AnswerOption' );
                            } else {
                                $externalValues = $this->getExternalValuesFor( $columnOption );
                            }

                            if(strpos($columnQuestionKey, 'sample-') !== false){
                                $colQuestionId = str_replace('sample-','', $columnQuestionKey);
                            } else if (strpos($columnQuestionKey, '-') !== false) {
                                //check for row matrix options
	                            $explodedContent = explode('-', $columnQuestionKey);
	                            $colQuestionId = end($explodedContent);
                            } else {
                                $colQuestionId = 0;
                            }

                            $responseFrequency[$rowQuestionKey][$columnQuestionKey][$rowOptionKey]['f'] = 0;
                            $responseFrequency[$rowQuestionKey][$columnQuestionKey][$rowOptionKey]['fx'] = 0;
                            $responseFrequency[$rowQuestionKey][$columnQuestionKey][$rowOptionKey]['fx2'] = 0;

                            foreach ($columnOption as $columnOptionKey => $colOpt) {
                                if(strpos($columnQuestionKey, 'sample-') !== false){
                                    $responseColumnOptionKey = 'sample-'.$columnOptionKey;
                                } else if (isset($colQuestionId) && $colQuestionId != 0) {
                                    $responseColumnOptionKey = $colQuestionId . '-' . $columnOptionKey;
                                } else {
                                    $responseColumnOptionKey = $columnOptionKey;
                                }

                                if (isset($responses[$responseRowOptionKey][$responseColumnOptionKey]['count']) && !empty($responses[$responseRowOptionKey][$responseColumnOptionKey]['count'])) {
                                    $responseFrequency[$rowQuestionKey][$columnQuestionKey][$rowOptionKey][$columnOptionKey] = $responses[$responseRowOptionKey][$responseColumnOptionKey]['count'];
                                    if( isset( $externalValues[$columnOptionKey]) ) {
                                        if( isset($externalValues[$columnOptionKey]) && (!is_null($externalValues[$columnOptionKey]) || $externalValues[$columnOptionKey] != '') ) {
                                            $exValue = $externalValues[$columnOptionKey];
                                            $exValue = str_replace(',', '.', $exValue);
                                            $responseFrequency[$rowQuestionKey][$columnQuestionKey][$rowOptionKey]['f'] += $responses[$responseRowOptionKey][$responseColumnOptionKey]['count'];
                                            $responseFrequency[$rowQuestionKey][$columnQuestionKey][$rowOptionKey]['fx'] += $exValue * $responses[$responseRowOptionKey][$responseColumnOptionKey]['count'];
                                            $responseFrequency[$rowQuestionKey][$columnQuestionKey][$rowOptionKey]['fx2'] += $exValue * $exValue * $responses[$responseRowOptionKey][$responseColumnOptionKey]['count'];
                                        }
                                    }
                                } else {
                                    $responseFrequency[$rowQuestionKey][$columnQuestionKey][$rowOptionKey][$columnOptionKey] = 0;
                                }
                                if (isset($responseOptionTotalColumnFrequency[$rowQuestionKey][$responseColumnOptionKey]['count']) && !empty($responseOptionTotalColumnFrequency[$rowQuestionKey][$responseColumnOptionKey]['count'])) {
                                    $responseInitialRowTotal[$rowQuestionKey][$columnQuestionKey][$columnOptionKey] = $responseOptionTotalColumnFrequency[$rowQuestionKey][$responseColumnOptionKey]['count'];
                                } else {
                                    $responseInitialRowTotal[$rowQuestionKey][$columnQuestionKey][$columnOptionKey] = 0;
                                }

                                if (!isset($rowResponseFrequency[$rowQuestionKey]['total']) || empty($rowResponseFrequency[$rowQuestionKey]['total'])) {
                                    $rowResponseFrequency[$rowQuestionKey]['total'] = 0;
                                    $rowResponseFrequency[$rowQuestionKey]['fx'] = 0;
                                    $rowResponseFrequency[$rowQuestionKey]['fx2'] = 0;
                                }

                                if (!isset($rowResponseFrequency[$rowQuestionKey][$rowOptionKey]) || empty($rowResponseFrequency[$rowQuestionKey][$rowOptionKey])) {
                                    $rowResponseFrequency[$rowQuestionKey][$rowOptionKey] = $responseOptionTotalFrequency[$responseRowOptionKey]['count'];
                                    $rowResponseFrequency[$rowQuestionKey]['total'] += $rowResponseFrequency[$rowQuestionKey][$rowOptionKey];
                                    if( isset( $externalValues[$columnOptionKey]) && (!is_null($externalValues[$columnOptionKey]) || $externalValues[$columnOptionKey] != '') ) {
                                        $rowResponseFrequency[$rowQuestionKey]['fx'] += $externalValues[$columnOptionKey] * $rowResponseFrequency[$rowQuestionKey][$rowOptionKey];
                                        $rowResponseFrequency[$rowQuestionKey]['fx2'] += $externalValues[$columnOptionKey] * $externalValues[$columnOptionKey] * $rowResponseFrequency[$rowQuestionKey][$rowOptionKey];
                                    }
                                }
                            }

                            unset($colQuestionId);
                        }
                    } else {
                        if( $system == 'legacy' ) {
                            $externalValues = $this->getExternalValuesFor( $rowOption, 'AnswerOption' );
                        } else {
                            $externalValues = $this->getExternalValuesFor( $rowOption );
                        }
                        if (isset($responseOptionTotalFrequency[$responseRowOptionKey]['count']) && !empty($responseOptionTotalFrequency[$responseRowOptionKey]['count'])) {
                            $responseFrequency[$rowQuestionKey][$rowOptionKey] = $responseOptionTotalFrequency[$responseRowOptionKey]['count'];
                        } else {
                            $responseFrequency[$rowQuestionKey][$rowOptionKey] = 0;
                        }

                        if (isset($responseOptionTotalColumnFrequency[$rowQuestionKey][$responseRowOptionKey]['count']) && !empty($responseOptionTotalColumnFrequency[$rowQuestionKey][$responseRowOptionKey]['count'])) {
                            $responseInitialRowTotal[$rowQuestionKey][$rowOptionKey] = $responseOptionTotalColumnFrequency[$rowQuestionKey][$responseRowOptionKey]['count'];
                        } else {
                            $responseInitialRowTotal[$rowQuestionKey][$rowOptionKey] = 0;
                        }
                        if (!isset($rowResponseFrequency[$rowQuestionKey]['total']) || empty($rowResponseFrequency[$rowQuestionKey]['total'])) {
                            $rowResponseFrequency[$rowQuestionKey]['total'] = 0;
                            $rowResponseFrequency[$rowQuestionKey]['fx'] = 0;
                            $rowResponseFrequency[$rowQuestionKey]['fx2'] = 0;
                        }

                        if (
                            (!isset($rowResponseFrequency[$rowQuestionKey][$rowOptionKey]) || empty($rowResponseFrequency[$rowQuestionKey][$rowOptionKey]))
                            && isset($responseOptionTotalFrequency[$responseRowOptionKey]) // intended to remove open question that is not numeric
                        ) {
                            $rowResponseFrequency[$rowQuestionKey][$rowOptionKey] = $responseOptionTotalFrequency[$responseRowOptionKey]['count'];
                            $rowResponseFrequency[$rowQuestionKey]['total'] += $rowResponseFrequency[$rowQuestionKey][$rowOptionKey];
                            if( isset($externalValues[$rowOptionKey]) ) {
                                $rowResponseFrequency[$rowQuestionKey]['fx'] += $externalValues[$rowOptionKey] * $rowResponseFrequency[$rowQuestionKey][$rowOptionKey];
                                $rowResponseFrequency[$rowQuestionKey]['fx2'] += $externalValues[$rowOptionKey] * $externalValues[$rowOptionKey] * $rowResponseFrequency[$rowQuestionKey][$rowOptionKey];
                            }
                        }
                    }
                }
                unset($rowQuestionId);
            }

            $optimizedResponse['responseInitialRowTotal'] = $responseInitialRowTotal;
            $optimizedResponse['rowResponseFrequency'] = $rowResponseFrequency;
            $optimizedResponse['responseFrequency'] = $responseFrequency;
        } else {
            $optimizedResponse['responseInitialRowTotal'] = null;
            $optimizedResponse['rowResponseFrequency'] = null;
            $optimizedResponse['responseFrequency'] = null;
        }

        return $optimizedResponse;
    }

}

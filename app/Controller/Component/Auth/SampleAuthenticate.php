<?php

App::uses('BaseAuthenticate', 'Controller/Component/Auth');

class SampleAuthenticate extends BaseAuthenticate {

    public function authenticate(CakeRequest $request, CakeResponse $response) {
        return $this->getUser($request);
    }

    public function getUser(CakeRequest $request) {
        if (!isset($request['data']['Sample']) || empty($request['data']['Sample'])) {
            return false;
        }
       
        $authenticatedSample = ClassRegistry::init('Sample')->find('first', array(
            'conditions' => array(
                'Sample.email' => $request['data']['Sample']['email'],
                'Sample.password' => AuthComponent::password($request['data']['Sample']['password'])
            )
        ));
      
        if (!isset($authenticatedSample) || empty($authenticatedSample)) {
            return false;
        }
        $authenticatedSample['Sample']['group_id'] = 5;
        return $authenticatedSample['Sample'];
    }

}
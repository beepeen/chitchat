<?php

/*
 * Image Upload Component
 * @author: BeepeEn ManandHar
 * Custom made
 * 
 */

class ImageUploadComponent extends Object {

    function initialize() {
        
    }

    function beforeRender() {
        
    }

    function shutdown() {
        
    }

    function startup(&$controller) {
        $this->Controller = & $controller;
    }

    function beforeRedirect() {
        
    }

    /*
     * Upload Image 
     * @prarams :$model, $id , $field , $albumName
     */

    function upload_images($model, $id, $field, $resize = true) {
        $DestinationDirectory = WWW_ROOT . 'files' . DS . $model . DS . $id . DS;
        if (!file_exists($DestinationDirectory)) {
            $oldmask = umask(0);
            mkdir($DestinationDirectory, 0777, true);
            umask($oldmask);
        }
        $DestinationDirectoryThumb = WWW_ROOT . 'files' . DS . $model . DS . $id . DS . 'thumb' . DS;
        if (!file_exists($DestinationDirectoryThumb)) {
            $oldmask = umask(0);
            mkdir($DestinationDirectoryThumb, 0777, true);
            umask($oldmask);
        }
        /* if (!file_exists($DestinationDirectory . "crop" . DS)) {
          @mkdir($DestinationDirectory . "crop" . DS, 0777, true);
          } */
        //chmod($DestinationDirectory, 0777);
        //Some Settings
        $ThumbSquareSize = 200; //Thumbnail will be 200x200
        $BigImageMaxSize = 1024; //Image Maximum height or width
        $ThumbPrefix = "thumb_"; //Normal thumb Prefix
        $Quality = 100;
        $crop = false;
        // check $_FILES['ImageFile'] array is not empty
        // "is_uploaded_file" Tells whether the file was uploaded via HTTP POST
        if (!isset($_FILES[$field]) || !is_uploaded_file($_FILES[$field]['tmp_name'])) {
            //die('Something went wrong with Upload!'); // output error when above checks fail.
            return array('error' => 'Image not selected');
        }

        // Random number for both file, will be added after image name
        $RandomNumber = rand(0, 9999999999);

        // Elements (values) of $_FILES['ImageFile'] array
        //let's access these values by using their index position
        $ImageName = str_replace(' ', '-', strtolower($_FILES[$field]['name']));
        $ImageSize = $_FILES[$field]['size']; // Obtain original image size
        $TempSrc = $_FILES[$field]['tmp_name']; // Tmp name of image file stored in PHP tmp folder
        $ImageType = $_FILES[$field]['type']; //Obtain file type, returns "image/png", image/jpeg, text/plain etc.
        //Let's use $ImageType variable to check wheather uploaded file is supported.
        //We use PHP SWITCH statement to check valid image format, PHP SWITCH is similar to IF/ELSE statements
        //suitable if we want to compare the a variable with many different values
        switch (strtolower($ImageType)) {
            case 'image/png':
                $CreatedImage = imagecreatefrompng($_FILES[$field]['tmp_name']);
                break;
            case 'image/gif':
                $CreatedImage = imagecreatefromgif($_FILES[$field]['tmp_name']);
                break;
            case 'image/jpeg':
            case 'image/pjpeg':
                $CreatedImage = imagecreatefromjpeg($_FILES[$field]['tmp_name']);
                break;
            default:
                return array('error' => 'Sorry. This file type is not supported.');
        }

        //PHP getimagesize() function returns height-width from image file stored in PHP tmp folder.
        //Let's get first two values from image, width and height. list assign values to $CurWidth,$CurHeight
        list($CurWidth, $CurHeight) = getimagesize($TempSrc);

        //Get file extension from Image name, this will be re-added after random name
        $ImageExt = substr($ImageName, strrpos($ImageName, '.'));
        $ImageExt = str_replace('.', '', $ImageExt);

        //remove extension from filename
        $ImageName = preg_replace("/\\.[^.\\s]{3,4}$/", "", $ImageName);

        //Construct a new image name (with random number added) for our new image.
        $NewImageName = $ImageName . '-' . $RandomNumber . '.' . $ImageExt;
        //set the Destination Image
        $thumb_DestRandImageName = $DestinationDirectoryThumb . $ThumbPrefix . $NewImageName; //Thumb name
        $DestRandImageName = $DestinationDirectory . $NewImageName; //Name for Big Image
        //Resize image to our Specified Size by calling resizeImage function.
        if ($this->resizeImage($CurWidth, $CurHeight, $BigImageMaxSize, $DestRandImageName, $CreatedImage, $Quality, $ImageType)) {

            //Create a square Thumbnail right after, this time we are using cropImage() function
            if ($crop == true) {
                if (!$this->cropImageFixed($CurWidth, $CurHeight, '200', '200', $thumb_DestRandImageName, $CreatedImage, $Quality, $ImageType)) {
                    return array('error' => 'Error Creating thumbnail!');
                }
            } else {
                if (!$this->resizeImage($CurWidth, $CurHeight, $ThumbSquareSize, $thumb_DestRandImageName, $CreatedImage, $Quality, $ImageType)) {
                    return array('error' => 'Error Creating thumbnail!');
                }
            }
            /*
              At this point we have succesfully resized and created thumbnail image
              We can render image to user's browser or store information in the database
              For demo, we are going to output results on browser.
             */
            /* echo '<table width="100%" border="0" cellpadding="4" cellspacing="0">';
              echo '<tr>';
              echo '<td align="center"><img src="uploads/'.$ThumbPrefix.$NewImageName.'" alt="Thumbnail"></td>';
              echo '</tr><tr>';
              echo '<td align="center"><img src="uploads/'.$NewImageName.'" alt="Resized Image"></td>';
              echo '</tr>';
              echo '</table>'; */


            //$mainFolder = 'http://' . $_SERVER['SERVER_NAME'] . Router::url('/').'files'.'/'.$model . '/' . $id .'/'.$albumName.'/';
            //return array('success'=>'Succesfully Uploaded!','preview'=>'<img src="'.$mainFolder.$NewImageName.'" style="width:120px;height:88px">','filename'=>$NewImageName);
            //return array('success'=>'Succesfully Uploaded!','preview'=> $mainFolder.$NewImageName,'filename'=>$NewImageName);
            $thumbFolder = 'http://' . $_SERVER['SERVER_NAME'] . Router::url('/') . 'files' . '/' . $model . '/' . $id . '/' . 'thumb' . '/';
            return array('success' => 'Succesfully Uploaded!', 'preview' => $thumbFolder . $ThumbPrefix . $NewImageName, 'filename' => $NewImageName);
        } else {
            return array('error' => 'Resize Error!');
        }
    }

    /*
     * Resize Image
     * This function will proportionally resize image
     * @params:$CurWidth,$CurHeight,$MaxSize,$DestFolder,$SrcImage,$Quality,$ImageType
     * @return bool
     */

    function resizeImage($CurWidth, $CurHeight, $MaxSize, $DestFolder, $SrcImage, $Quality, $ImageType) {
        //Check Image size is not 0
        if ($CurWidth <= 0 || $CurHeight <= 0) {
            return false;
        }

        //Construct a proportional size of new image
        /* if($CurWidth>$CurHeight){
          $MaxSize = $CurWidth;
          }else{
          $MaxSize = $CurHeight;
          } */
        if ($CurWidth > $CurHeight && $CurWidth < 200) {
            $MaxSize = $CurWidth;
        }
        if ($CurWidth < $CurHeight && $CurHeight < 200) {
            $MaxSize = $CurHeight;
        }

        $ImageScale = min($MaxSize / $CurWidth, $MaxSize / $CurHeight);
        $NewWidth = ceil($ImageScale * $CurWidth);
        $NewHeight = ceil($ImageScale * $CurHeight);
        $NewCanves = imagecreatetruecolor($NewWidth, $NewHeight);

        // Resize Image
        if (imagecopyresampled($NewCanves, $SrcImage, 0, 0, 0, 0, $NewWidth, $NewHeight, $CurWidth, $CurHeight)) {
            switch (strtolower($ImageType)) {
                case 'image/png':
                    imagepng($NewCanves, $DestFolder);
                    break;
                case 'image/gif':
                    imagegif($NewCanves, $DestFolder);
                    break;
                case 'image/jpeg':
                case 'image/pjpeg':
                    imagejpeg($NewCanves, $DestFolder, $Quality);
                    break;
                default:
                    return false;
            }
            //Destroy image, frees up memory
            if (is_resource($NewCanves)) {
                imagedestroy($NewCanves);
            }
            return true;
        }
    }

    /*
     * Crop Image
     * //This function corps image to create exact square images, no matter what its original size!
     * @params:$CurWidth,$CurHeight,$MaxSize,$DestFolder,$SrcImage,$Quality,$ImageType
     * @return bool
     */

    function cropImage($CurWidth, $CurHeight, $iSize, $DestFolder, $SrcImage, $Quality, $ImageType) {
        //Check Image size is not 0
        if ($CurWidth <= 0 || $CurHeight <= 0) {
            return false;
        }

        //abeautifulsite.net has excellent article about "Cropping an Image to Make Square"
        //http://www.abeautifulsite.net/blog/2009/08/cropping-an-image-to-make-square-thumbnails-in-php/
        if ($CurWidth > $CurHeight) {
            $y_offset = 0;
            $x_offset = ($CurWidth - $CurHeight) / 2;
            $square_size = $CurWidth - ($x_offset * 2);
        } else {
            $x_offset = 0;
            $y_offset = ($CurHeight - $CurWidth) / 2;
            $square_size = $CurHeight - ($y_offset * 2);
        }

        $NewCanves = imagecreatetruecolor($iSize, $iSize);
        if (imagecopyresampled($NewCanves, $SrcImage, 0, 0, $x_offset, $y_offset, $iSize, $iSize, $square_size, $square_size)) {
            switch (strtolower($ImageType)) {
                case 'image/png':
                    imagepng($NewCanves, $DestFolder);
                    break;
                case 'image/gif':
                    imagegif($NewCanves, $DestFolder);
                    break;
                case 'image/jpeg':
                case 'image/pjpeg':
                    imagejpeg($NewCanves, $DestFolder, $Quality);
                    break;
                default:
                    return false;
            }
            //Destroy image, frees up memory
            if (is_resource($NewCanves)) {
                imagedestroy($NewCanves);
            }
            return true;
        }
    }

    function cropImageFixed($CurWidth, $CurHeight, $iSizeW, $iSizeH, $DestFolder, $SrcImage, $Quality, $ImageType) {
        //Check Image size is not 0
        if ($CurWidth <= 0 || $CurHeight <= 0) {
            return false;
        }

        //abeautifulsite.net has excellent article about "Cropping an Image to Make Square"
        //http://www.abeautifulsite.net/blog/2009/08/cropping-an-image-to-make-square-thumbnails-in-php/
        if ($CurWidth > $CurHeight) {
            $y_offset = 0;
            $x_offset = ($CurWidth - $CurHeight) / 2;
            $square_size = $CurWidth - ($x_offset * 2);
        } else {
            $x_offset = 0;
            $y_offset = ($CurHeight - $CurWidth) / 2;
            $square_size = $CurHeight - ($y_offset * 2);
        }

        $NewCanves = imagecreatetruecolor($iSizeW, $iSizeH);
        if (imagecopyresampled($NewCanves, $SrcImage, 0, 0, $x_offset, $y_offset, $iSizeW, $iSizeH, $square_size, $square_size)) {
            switch (strtolower($ImageType)) {
                case 'image/png':
                    imagepng($NewCanves, $DestFolder);
                    break;
                case 'image/gif':
                    imagegif($NewCanves, $DestFolder);
                    break;
                case 'image/jpeg':
                case 'image/pjpeg':
                    imagejpeg($NewCanves, $DestFolder, $Quality);
                    break;
                default:
                    return false;
            }
            //Destroy image, frees up memory
            if (is_resource($NewCanves)) {
                imagedestroy($NewCanves);
            }
            return true;
        }
    }

    /*
     * get File extension
     */

    function getFileExtension($str) {

        $i = strrpos($str, ".");
        if (!$i) {
            return "";
        }
        $l = strlen($str) - $i;
        $ext = substr($str, $i + 1, $l);
        return $ext;
    }

}
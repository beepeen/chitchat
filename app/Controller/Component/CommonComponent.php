<?php

App::uses('Component', 'Controller');
App::uses('Controller', 'Controller');

class CommonComponent extends Component {

    var $components = array('Email');

    public function initialize(&$controller) {
        $this->Email->reset();
        //$this->controller = &$controller;
    }

    /* function startup(&$controller) {
      $this->controller = &$controller;
      } */

    /**
     * clean slug function
     * @param string $string	
     * @return string
     */
    public function getSlug($string, $concatinator = '-', $enableSpecialChars = false) {
        if($enableSpecialChars == false){
            $new_string = preg_replace("/[^a-zA-Z0-9-\@\$ \s]/", "", strtolower(strip_tags($string)));
        } else {
            $new_string = str_replace(array("\r\n", "\r", "\n", " "), "_", $string);
            $new_string = strtolower(strip_tags($new_string));
        }
        $rep_string = str_replace(" ", $concatinator, trim($new_string));
        $rep_string = preg_replace('/-+/', $concatinator, $rep_string);
        $rep_string = preg_replace('/\'/', '', $rep_string);
        $rep_string = preg_replace('/"/', '', $rep_string);
        return $rep_string;
    }

    /**
     * slug generator function
     * @param array $model	
     * @param string $field	
     * @param string $title	
     * @param int $id	
     * @return boolean
     */
    public function makeSlug($model, $field, $title, $id) {
        $slug = $this->getSlug($title);
        $this->$model = ClassRegistry::init($model);
        $options = array('conditions' => array('not' => array($model . '.' . $this->$model->primaryKey => $id), $field => $slug));
        //$options = array('conditions' => array($model . '.' . $this->$model->primaryKey => $id, $field => $slug));
        if ($this->$model->find('first', $options)) {
            $slug = $slug . $id;
        }
        $this->$model->id = $id;
        $this->$model->saveField($field, $slug);
        return true;
    }

    /**
     * Email function
     * @param string $fromName	
     * @param string $fromEmail	
     * @param string $receiverEmail	
     * @param string $subject
     * @param string $bodyTemplate
     * @param array $info
     * @param string $replyTo	
     * @return boolean
     */
    public function sendEmail($fromName = "", $fromEmail = "", $receiverEmail, $replyTo = "", $subject, $content, $attachment = null, $bcc = array()) {
        if ($fromName != "" && $fromEmail != "")
            $from = $fromName . " <" . $fromEmail . ">";
        else
            $from = 'Chitchat <noreply@chitchat.com>'; //email sent from the site

        $this->Email->reset();
        $this->Email->to = $receiverEmail;
        $this->Email->bcc = $bcc;

        $this->Email->subject = $subject;
        $this->Email->from = $from;
        if ($replyTo != "") {
            $this->Email->replyTo = $replyTo;
        } else {
            $this->Email->replyTo = 'noreply@chitchat.com';
        }

        if (!empty($attachment)) {
            $this->Email->attachments = $attachment;
        }

        $this->Email->sendAs = 'both';
        if ($this->Email->send($content)){
            $this->log($content, 'debug');
            return true;
        } else {
            return false;
        }
    }

    /**
     * content cleaner function
     * @param string $string	
     * @param int $limit	
     * @return string
     */
    public static function getCleanData($string, $limit = 0) {
        if ($limit != 0 && strlen(strip_tags($string)) > $limit) {
            $return_string = substr(trim(strip_tags($string)), 0, $limit);
        }
        else
            $return_string = trim(strip_tags($string));
        return $return_string;
    }

}

?>
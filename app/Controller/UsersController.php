<?php

App::uses('CleanAdminAppController', 'CleanAdmin.Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class UsersController extends CleanAdminAppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');

    /**
     * login method
     *
     * @return void
     */
    public function login() {
        $this->layout = 'login';
        if ($this->Auth->login()) {
            $error = false;
            $msg = '';
            if (!in_array($this->Auth->user('group_id'), array(4))) {
                $error = true;
                $msg = __('Email or password is incorrect');
            }
            if ($this->Auth->user('status') == 0) {
                $error = true;
                $msg = __('User is not active');
            }

            if (isset($error) && $error == true) {
                $this->Session->setFlash($msg, 'error_message', array(), 'auth');
                $this->redirect(array('action' => 'logout'));
            }

            $user = array(
                'user_id' => $this->Auth->user('id'),
                'sessionid' => $this->Session->id(),
                'ip' => $this->User->get_ip(),
                'referrer' => $this->request->referer(),
                'useragent' => env('HTTP_USER_AGENT')
            );
            $this->User->Userlog->createUserLog($user);

            //$userInfo = $this->Session->read('Auth.User');
            $redirectURL = array('controller' => 'dashboards', 'action' => 'index');
            return $this->redirect($redirectURL);
        } else {
            if ($this->request->is('post') || $this->request->is('put')) {
                $this->Session->setFlash(__('Email or password is incorrect'), 'error_message', array(), 'auth');
            }
        }
    }

    /**
     * logout method
     * Logout Area for Users
     * @return void
     */
    public function logout() {
        /*if (isset($_SESSION['KCFINDER'])) {
            $this->Session->delete('KCFINDER'); //kcfinder disable
        }*/

        $this->Session->delete('Alaxos.Acl.permissions' . AuthComponent::$sessionKey);
        $this->redirect($this->Auth->logout());
    }

    /**
     * register account method
     *
     * @return void
     */
    public function register() {

        if ($this->request->is('post')) {
            $this->request->data['User']['status'] = 1;
            $this->request->data['User']['username'] = $this->request->data['User']['email'];
            $this->User->validator()->remove('username');
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                //$user_id = $this->User->id;
                //login user and redirect to landing page
                if ($this->Auth->login()) {
                    $this->redirect($this->Auth->redirect());
                }
                //$this->Session->setFlash(__('The user has been saved.'), 'success_message');
                //return $this->redirect(array('controller' => 'dashboards', 'action' => 'index'));
            } else {
                $errors = $this->User->validationErrors;
                $this->set('errors', $errors);
                //$this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'error_message');
            }
        }
    }

    /**
     * user profile function
     */
    public function profile() {

        $userId = $this->Auth->user('id');
        if (!$this->User->exists($userId)) {
            throw new NotFoundException(__('Invalid user'));
        }

        //$user = $this->Auth->user();
        $user = $this->User->getUserData($userId);
        $contactList = $this->User->getRecentUsers($userId);

        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['User']['id'] = $userId;
            if (isset($this->request->data['User']['password']) && $this->request->data['User']['password'] == '') {
                unset($this->request->data['User']['password']);
                unset($this->request->data['User']['confirm_password']);
            }

            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('Your profile has been updated.'), 'success_message');
                return $this->redirect(array('action' => 'profile'));
            } else {
                $errors = $this->User->validationErrors;
                $this->set('errors', $errors);
                $this->Session->setFlash(__('Your profile could not be saved. Please, try again.'), 'error_message');
            }
        } else {
            //set values for user data
            $this->request->data['User'] = $user;
        }
        $this->set(compact('user', 'contactList'));
    }

    /**
     * User Forgot Password method
     *
     * @throws MethodNotAllowedException
     * @throws NotFoundException
     * @return void
     * @author BeepeEn ManandHar
     */
    public function forgot_password() {
        $this->layout = 'login';
        if (!empty($this->data)) {
            if (empty($this->data['User']['email'])) {
                $this->Session->setFlash(__('Please provide your email that you used to register with us.'), 'error_message');
            } else {
                $email = $this->data['User']['email'];
                $fu = $this->User->find('first', array('conditions' => array('User.email' => $email)));
                if ($fu) {
                    $key = Security::hash(String::uuid(), 'sha512', true);
                    $url = Router::url(array('controller' => 'users', 'action' => 'reset_password', 'plugin' => false), true) . '/' . $key;
                    $resetLink = wordwrap($url, 1000);
                    $fu['User']['reset_token'] = $key;
                    $this->User->id = $fu['User']['id'];
                    if ($this->User->saveField('reset_token', $fu['User']['reset_token'])) {

                        //notify user for reset token
                        $receiverEmail = $fu['User']['email'];

                        //here goes message template
                        $subject = __('Chitchat password reset request');
                        $greetingMsg =  (isset($fu['User']['name']) && $fu['User']['name']!='') ? $fu['User']['name'] : $fu['User']['email'];
                        $content = '<p>Hello ' . $greetingMsg .',<br /><br />
                                    You asked us to send a password reset link for "' . $fu['User']['email'] . '"<br />
                                    Please click on following link to reset your password.<br/>
                                    ' . $resetLink . ' <br /><br />
                                    Regards, <br />
                                    Chitchat</p>';

                        $this->Common = $this->Components->load('Common');
                        if ($this->Common->sendEmail('', '', $receiverEmail, '', $subject, $content)) {
                            //$this->Session->setFlash(__('Check your email for reset link to reset your password.'), 'success_message');
                            $this->redirect($resetLink);
                        } else {
                            $this->Session->setFlash(__('Error sending reset link. Please try again later.'), 'error_message');
                        }
                    } else {
                        $this->Session->setFlash(__('Error generating reset link. Please try again later.'), 'error_message');
                    }
                } else {
                    $this->Session->setFlash(__("Sorry, Chitchat doesn't recognize that email."), 'error_message');
                }
            }
        }
    }

    /**
     * User Password Reset method
     *
     * @throws MethodNotAllowedException
     * @throws NotFoundException
     * @param varchar $token hashed token provided in reset link
     * @return void
     * @author BeepeEn ManandHar
     */
    public function reset_password($token = null) {
        $this->layout = 'login';
        if (!empty($token)) {
            $user = $this->User->findByreset_token($token);
            if ($user) {
                $this->User->id = $user['User']['id'];
                if (!empty($this->data)) {
                    $this->User->data = $this->data;
                    $this->User->data['User']['email'] = $user['User']['email'];
                    if ($this->User->validates(array('fieldList' => array('password', 'confirm_password')))) {
                        if ($this->User->save($this->User->data)) {
                            $this->Auth->login($user['User']);
                            $this->User->saveField('reset_token', '');
                            $this->Session->setFlash(__('Your password has been updated successfully.'), 'success_message');
                            $this->redirect(Router::url(array('controller' => 'users', 'action' => 'login', 'plugin' => false), true));
                        }
                    } else {
                        $this->set('errors', $this->User->invalidFields());
                    }
                }
                $this->set('user', $user);
            } else {
                $this->Session->setFlash(__('Invalid token!'), 'error_message');
                $this->redirect(Router::url(array('controller' => 'users', 'action' => 'forgot_password', 'plugin' => false), true));
            }
        } else {
            $this->redirect('/');
        }
    }

    /**
     * ajax_search
     * @return CakeResponse
     */
    public function ajax_search() {
        $this->layout = false;
        $this->autoRender = false;
        if ($this->request->is(array('post', 'put'))) {

            $status = false;
            $query = $this->request->data['User']['keyword'];
            $msg = __('There was problem searching for existing users.');

            //Exclude own user id when searching user
            $options = array(
                'conditions' => array(
                    'User.id !=' => $this->Session->read('Auth.User.id'),
                    'User.group_id ' => 4,
                    'OR' => array(
                        'User.name LIKE' => "%{$query}%",
                        'User.username LIKE' => "%{$query}%",
                    )
                ),
                'fields' => array(
                    'User.id',
                    'User.name',
                    'User.username',
                    'User.email',
                )
            );

            $users = $this->User->getUsers('all', $options);
            $status = true;
            $msg = __('Success.');

            $view = new View($this, false);
            $contactListView = $view->element('contact_list', array('contactList' => $users));

            $this->response->type('json');
            $this->response->body(json_encode(array('status' => $status, 'msg' => $msg, 'users' => $users, 'contactListView' => $contactListView)));
            return $this->response;
        }
    }

    /**
     * user view_profile function
     */
    public function view_profile($userId) {

        if (!$this->User->exists($userId)) {
            throw new NotFoundException(__('Invalid user'));
        }

        $user = $this->User->getUserData($userId);
        $contactList = $this->User->getRecentUsers($this->Auth->user('id'));
        $this->set(compact('user', 'contactList'));
    }
}

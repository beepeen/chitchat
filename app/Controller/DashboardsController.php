<?php

App::uses('CleanAdminAppController', 'CleanAdmin.Controller');

/**
 * Dashboards Controller
 *
 * @property Dashboard $Dashboard
 */

class DashboardsController extends CleanAdminAppController {
    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session');

    public function index() {

        $this->loadModel('User');
        $user = $this->Auth->user();
        $userId = $this->Auth->user('id');

        $contactList = $this->User->getRecentUsers($userId);
        $this->set(compact('user', 'contactList'));
    }

    /**
     * Ajax chat viewer
     * @return void
     *
     */
    public function ajax_chat_viewer() {

        $this->autoRender = false;
        $this->autoLayout = false;

        $response['status'] = false;
        $response['message'] = __('You are not authorized to access this location.');

        if ($this->request->is('ajax')) {
            $senderId = $this->request->data('senderId');
            $receiverId = $this->request->data('receiverId');

            $this->loadModel('User');
            $receiver = $this->User->getUserData($receiverId);

            $view = new View($this, false);
            $response['view'] = $view->element('chat_window', array(
                'senderId' => $senderId,
                'receiverId' => $receiverId,
                'receiver' => $receiver
            ));

            $response['status'] = true;
            $response['message'] = __('Success.');
        }
        echo json_encode($response);
        exit;
    }

    /**
     * Ajax chat
     * @return void
     *
     */
    public function ajax_chat() {

        $this->autoRender = false;
        $this->autoLayout = false;

        $response['status'] = false;
        $response['message'] = __('You are not authorized to access this location.');

        if ($this->request->is('ajax')) {
            pr('testasd');
            /*$senderId = $this->request->data('senderId');
            $receiverId = $this->request->data('receiverId');

            $this->loadModel('User');
            $receiver = $this->User->getUserData($receiverId);

            $view = new View($this, false);
            $response['view'] = $view->element('chat_window', array(
                'senderId' => $senderId,
                'receiverId' => $receiverId,
                'receiver' => $receiver
            ));

            $response['status'] = true;
            $response['message'] = __('Success.');*/
        }
        echo json_encode($response);
        exit;
    }
}
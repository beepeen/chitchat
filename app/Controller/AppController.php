<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');

App::uses('ChitchatAro', 'Lib/Chitchat/Aro');
/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public $components = array(
        'Acl',
        'Auth' => array(
            'authorize' => array(
                'Actions' => array('actionPath' => 'controllers')
            )
        ),
        'Session', 'Cookie', 'RequestHandler'
    );
    public $helpers = array('Html', 'Form', 'Session', 'ZoneAcl.ZoneAclHtml');

    public function beforeFilter() {

        $testIP = array('127.0.0.1'); //test ips
        $this->testIP = $testIP;

        if (in_array($_SERVER['REMOTE_ADDR'], $this->testIP)) {
            Configure::write('Config.language', 'eng');
            $this->environment = 'development';
        } else {
            Configure::write('Config.language', 'irl');
            $this->environment = 'production';
        }
        $this->set('language', Configure::read('Config.language'));

        $baseURL = Router::url('/', true);
        $this->set('baseUrl', $baseURL);

        $userInfo = $this->Session->read('Auth.User');

        $this->theme = "FrontEnd";
        AuthComponent::$sessionKey = 'Auth.User';
        $this->Auth->loginAction = array('controller' => 'users', 'action' => 'login', 'user' => false, 'plugin' => false);
        $this->Auth->logoutRedirect = array('controller' => 'users', 'action' => 'login', 'user' => false, 'plugin' => false);
        $this->Auth->loginRedirect = array('controller' => 'dashboards', 'action' => 'index', 'user' => false, 'plugin' => false);
        $this->Auth->unauthorizedRedirect = array('controller' => 'users', 'action' => 'login', 'plugin' => false);
        $this->Auth->authenticate = array('Form' => array('fields' => array('username' => 'email')));

        if ($this->action == "logout" && strpos($this->referer(), '/users/login') !== false) {
            $this->redirect(Router::url(array('controller' => 'users', 'action' => 'profile'), true));
        }

        $this->allowAccess();
        //if ($this->Session->read('Auth.User.id')) {
        //    $this->Auth->allow();
        //}

        // plug custom ARO    
        ZoneAcl::setAro(new ChitchatAro());
    }

    /*
     * Private allow action
     */
    private function allowAccess() {
        // this actually searches the URL to see what controller you're accessing, and allows actions for that controller.

        if (in_array($this->name, array('Pages'))) {
            $this->Auth->allow(array('home', 'promotion', 'display', 'offline', 'maintenance', 'unauthorized'));
        } elseif (in_array($this->name, array('Users'))) {
            $this->Auth->allow(array('register', 'login', 'logout', 'forgot_password', 'reset_password'));
        }

    }
}

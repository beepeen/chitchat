<?php

App::uses('Inflector', 'Utility');
App::uses('Router', 'Routing');
App::uses('ZoneAcl', 'ZoneAcl.Controller/Component/Acl');

class ZoneAclUtility {

	protected static $rootUrl;

	protected static function getRootUrl() {

		if (self::$rootUrl) {
			return self::$rootUrl;
		}
		
		self::$rootUrl = Router::url('/');

		return self::$rootUrl;
	}

	public static function convertUrlToAco($url, $actionPath = null) {
		$rootUrl = self::getRootUrl();
		$url = Router::url($url);
		$url = preg_replace("#^{$rootUrl}#", '/', $url, 1);
		$url = Router::parse($url);

		$aco = array();

		$actionPath = empty($actionPath) ? ZoneAcl::$actionPath : $actionPath;
		$actionPath = rtrim($actionPath, '/');

		$aco[] = $actionPath;

		if (isset($url['plugin'])) {
			$aco[] = Inflector::camelize($url['plugin']);
		}

		if (isset($url['controller'])) {
			$aco[] = Inflector::camelize($url['controller']);
		}

		if (isset($url['action'])) {
			$aco[] = $url['action'];
		}

		$aco = implode('/', $aco);
		return $aco;
	}

}

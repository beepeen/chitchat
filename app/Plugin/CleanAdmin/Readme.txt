
//Steps for using plugin
1.  Add a plugin in plugin folder. ALso add the ACL plugin. Initialize these plugin on bootstrap in your project
2.  Generate schema
        Console/cake schema create --plugin CleanAdmin

3.  Install required template
        Console/cake CleanAdmin.install

4.  Baking FOR CONTROLLER AND MODEL
    Console/cake CleanAdmin.ExtBake -p CleanAdmin

    Baking FOR VIEW
    Console/cake CleanAdmin.ExtBake 

    Mandatory Bake : Group User 

5.  Add this App controller.
    
    public $components = array(
        'Acl',
        'Auth' => array(
            'authorize' => array(
                'Actions' => array('actionPath' => 'controllers')
            )
        ),
        'Session',
        'Acl.AclManager'
    );
    public $helpers = array('Html', 'Form', 'Session','Acl.AclHtml');

    public function beforeFilter() {
        if (isset($this->request->params['admin'])) {
            $this->theme = "Admin";
            //Auth component
            AuthComponent::$sessionKey = 'Auth.Admin';
            $this->Auth->allow('login','logout','admin_add');
            //$this->Auth->allow();
            //Set Acl based Permission for the session
            $this->AclManager->set_session_permissions();
            //Configure AuthComponent
            $this->Auth->loginAction = array(
                'controller' => 'users',
                'action' => 'login'
            );
            $this->Auth->logoutRedirect = array(
                'controller' => 'users',
                'action' => 'login'
            );
            $this->Auth->loginRedirect = array(
                'controller' => 'users',
                'action' => 'index'
            );
            $this->Auth->flash['element'] = 'error_message';
        } else {
            $this->Auth->allow();
        }
    }




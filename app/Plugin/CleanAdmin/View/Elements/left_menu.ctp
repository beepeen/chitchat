<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<!-- Left side column. contains the logo and sidebar -->
<aside class="left-side sidebar-offcanvas">                
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <?php echo $this->Html->image('../img/avatar.png', array('class' => 'img-circle')) ?>
            </div>
            <div class="pull-left info">
                <p><?php echo __('Hello, Admin') ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
                <span class="input-group-btn">
                    <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form>
        <!-- /.search form -->
        <?php
        $leftMenus = array();
        $defaultIcon = "fa-bars";
        $leftMenus[] = array(
            'label' => __('Dashboard'),
            'link' => Router::url(array('controller' => 'users')),
            'icon' => 'fa-dashboard'
        );
        $leftMenus[] = array(
            'label' => __('Users'),
            'link' => '#',
            'icon' => 'fa-user',
            'submenu' => array(
                array(
                    'label' => __('List User'),
                    'link' => Router::url(array('controller' => 'users'))
                ),
                array(
                    'label' => __('Add User'),
                    'link' => Router::url(array('controller' => 'users','action' => 'add'))
                ),
                array(
                    'label' => __('List Group'),
                    'link' => Router::url(array('controller' => 'groups'))
                ),
                array(
                    'label' => __('Add Group'),
                    'link' => Router::url(array('controller' => 'users','action' => 'add'))
                )
            )
        );
        ?>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <?php foreach ($leftMenus as $leftMenu): ?>
                <li <?php echo (isset($leftMenu['submenu'])) ? 'class="treeview"' : ''; ?>>
                    <a href="<?php echo $leftMenu['link'] ?>">
                        <i class="fa <?php echo isset($leftMenu['icon'])?$leftMenu['icon']:$defaultIcon; ?>"></i> <span><?php echo $leftMenu['label'] ?></span>
                        <?php echo (isset($leftMenu['submenu'])) ? ' <i class="fa fa-angle-left pull-right"></i>' : ''; ?>

                    </a>
                    <?php if (isset($leftMenu['submenu'])): ?>
                        <ul class="treeview-menu">
                            <?php foreach ($leftMenu['submenu'] as $key => $subMenu):
                                ?> 
                                <li> 
                                    <a href="<?php echo $subMenu['link'] ?>"><i class="fa fa-angle-double-right"></i> <?php echo $subMenu['label'] ?></a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                </li>
            <?php endforeach; ?>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>


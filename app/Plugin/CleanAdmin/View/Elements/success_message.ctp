<div class="alert alert-success alert-message">

    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong><?php echo __('Success!') ?></strong>  <?php echo $message; ?>
</div>

<div class="alert alert-danger alert-dismissable">
    <i class="glyphicon glyphicon-ban-circle"></i>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <strong><?php echo __('Alert') ?>!</strong> <?php echo $message; ?>
</div>
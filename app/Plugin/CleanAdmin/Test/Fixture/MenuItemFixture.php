<?php
/**
 * MenuItemFixture
 *
 */
class MenuItemFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 250, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'type' => array('type' => 'integer', 'null' => false, 'default' => '0', 'length' => 2, 'comment' => '0 => link, 1=> CMS pages, 2=> categories'),
		'value' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 250, 'collate' => 'utf8_unicode_ci', 'comment' => 'this can be link category id or contetn id', 'charset' => 'utf8'),
		'menu_id' => array('type' => 'integer', 'null' => false, 'default' => '0'),
		'menut_item_id' => array('type' => 'integer', 'null' => false, 'default' => '0'),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'name' => 'Lorem ipsum dolor sit amet',
			'type' => 1,
			'value' => 'Lorem ipsum dolor sit amet',
			'menu_id' => 1,
			'menut_item_id' => 1,
			'created' => '2014-05-05 13:26:03',
			'modified' => '2014-05-05 13:26:03'
		),
	);

}

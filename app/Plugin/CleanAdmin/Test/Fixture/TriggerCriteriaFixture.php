<?php
/**
 * TriggerCriteriaFixture
 *
 */
class TriggerCriteriaFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'trigger_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'criteria_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'logic' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'trigger_id' => 1,
			'criteria_id' => 1,
			'logic' => 'Lorem ipsum dolor sit amet'
		),
	);

}

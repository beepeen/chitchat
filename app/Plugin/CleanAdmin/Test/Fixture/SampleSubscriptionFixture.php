<?php
/**
 * SampleSubscriptionFixture
 *
 */
class SampleSubscriptionFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'sample_subscription';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'survey_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'email' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'subscription' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 2, 'unsigned' => false, 'comment' => '(0=>unsubscribed, 1=>subscribed)'),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'survey_id' => 1,
			'email' => 'Lorem ipsum dolor sit amet',
			'subscription' => 1,
			'created' => '2015-03-23 10:54:10',
			'modified' => '2015-03-23 10:54:10'
		),
	);

}

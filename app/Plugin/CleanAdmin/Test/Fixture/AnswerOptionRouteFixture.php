<?php
/**
 * AnswerOptionRouteFixture
 *
 */
class AnswerOptionRouteFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'answer_option_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'question_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => 'question id to be routed'),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'answer_option_id' => 1,
			'question_id' => 1,
			'modified' => '2014-06-30 12:51:45',
			'created' => '2014-06-30 12:51:45'
		),
	);

}

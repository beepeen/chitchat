<?php
/**
 * AttributeFixture
 *
 */
class AttributeFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 250, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'description' => array('type' => 'text', 'null' => false, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'type' => array('type' => 'integer', 'null' => false, 'default' => '0', 'length' => 2, 'comment' => ' 0 => text, 1=> dropdown, 2=> textarea, 3 => yes/no, 4=> multiple select. 5=> file, 6=> numeric'),
		'required' => array('type' => 'integer', 'null' => false, 'default' => '0', 'length' => 2, 'comment' => '0 => No, 1=> Yes'),
		'use_in_search' => array('type' => 'integer', 'null' => false, 'default' => '0', 'length' => 2, 'comment' => '0 => No, 1=> Yes'),
		'use_in_detail' => array('type' => 'integer', 'null' => false, 'default' => '0', 'length' => 2, 'comment' => '0 => No, 1=> Yes'),
		'use_in_filter' => array('type' => 'integer', 'null' => false, 'default' => '0', 'length' => 2, 'comment' => '0 => No, 1 => Yes'),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'name' => 'Lorem ipsum dolor sit amet',
			'description' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'type' => 1,
			'required' => 1,
			'use_in_search' => 1,
			'use_in_detail' => 1,
			'use_in_filter' => 1,
			'created' => '2014-05-06 12:46:18',
			'modified' => '2014-05-06 12:46:18'
		),
	);

}

<?php

/**
 * CompanyFoldersUserFixture
 *
 */
class CompanyFoldersUserFixture extends CakeTestFixture {

    /**
     * Fields
     *
     * @var array
     */
    public $fields = array(
        'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
        'company_folder_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
        'user_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
        'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
        'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
        'indexes' => array(
            'PRIMARY' => array('column' => 'id', 'unique' => 1),
            'company_folder_id_company_group_id' => array('column' => array('company_folder_id', 'user_id'), 'unique' => 1),
            'company_folder_id' => array('column' => 'company_folder_id', 'unique' => 0),
            'user_id' => array('column' => 'user_id', 'unique' => 0)
        ),
        'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
    );

    /**
     * Records
     *
     * @var array
     */
    public $records = array(
        array(
            'id' => 1,
            'company_folder_id' => 1,
            'user_id' => 1,
            'created' => '2014-11-28 12:37:47',
            'modified' => '2014-11-28 12:37:47'
        ),
    );

}

<?php
/**
 * PipingVariableFixture
 *
 */
class PipingVariableFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'survey_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'sample_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'piping_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'piping_value' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'survey_id' => 1,
			'sample_id' => 1,
			'piping_id' => 1,
			'piping_value' => 'Lorem ipsum dolor sit amet',
			'created' => '2014-12-26 07:33:24',
			'modified' => '2014-12-26 07:33:24'
		),
	);

}

<?php
/**
 * QuestionFixture
 *
 */
class QuestionFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'name' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'question_type_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'question' => array('type' => 'text', 'null' => false, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'info' => array('type' => 'text', 'null' => false, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'width_format' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 2, 'unsigned' => false, 'comment' => '(0=> Pixel; 1 => Percentage)'),
		'width' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => '(percentage or pixel values)'),
		'required' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 2, 'unsigned' => false, 'comment' => '(0 => not required ; 1 => required)'),
		'required_msg' => array('type' => 'text', 'null' => false, 'default' => null, 'collate' => 'utf8_unicode_ci', 'comment' => '(message when question is mandatory)', 'charset' => 'utf8'),
		'response_type' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 2, 'unsigned' => false, 'comment' => '(0 => none, 1 => text, 2 => textarea, 3 => numeric)'),
		'row' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'column' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'maxlength' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => '(maximum no of characters allowed for a field)'),
		'min_value' => array('type' => 'float', 'null' => false, 'default' => null, 'unsigned' => false),
		'max_value' => array('type' => 'float', 'null' => false, 'default' => null, 'unsigned' => false),
		'prefix' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50, 'collate' => 'utf8_unicode_ci', 'comment' => '(prefix to be added in answer)', 'charset' => 'utf8'),
		'suffix' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50, 'collate' => 'utf8_unicode_ci', 'comment' => '(suffix to be added in answer)', 'charset' => 'utf8'),
		'explanation_type' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 2, 'unsigned' => false, 'comment' => '(0 => none, 1 => single line, 2 => multiple lines)'),
		'explanation_placeholder' => array('type' => 'text', 'null' => false, 'default' => null, 'collate' => 'utf8_unicode_ci', 'comment' => '(placeholder for single line and multiple lines)', 'charset' => 'utf8'),
		'display_image_text' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 2, 'unsigned' => false, 'comment' => '(0 => no image text ; 1 => display image text)'),
		'answer_order' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => '(0 => natural, 1 => random, 2 => alphabetic)[if natural; display order will be based on answer options orders]'),
		'display_order' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'comment' => '(ordering of questions)'),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'name' => 'Lorem ipsum dolor sit amet',
			'question_type_id' => 1,
			'question' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'info' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'width_format' => 1,
			'width' => 1,
			'required' => 1,
			'required_msg' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'response_type' => 1,
			'row' => 1,
			'column' => 1,
			'maxlength' => 1,
			'min_value' => 1,
			'max_value' => 1,
			'prefix' => 'Lorem ipsum dolor sit amet',
			'suffix' => 'Lorem ipsum dolor sit amet',
			'explanation_type' => 1,
			'explanation_placeholder' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'display_image_text' => 1,
			'answer_order' => 1,
			'display_order' => 1,
			'created' => '2014-05-28 13:15:47',
			'modified' => '2014-05-28 13:15:47'
		),
	);

}

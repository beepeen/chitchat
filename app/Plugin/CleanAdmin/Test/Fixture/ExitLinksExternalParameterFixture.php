<?php
/**
 * ExitLinksExternalParameterFixture
 *
 */
class ExitLinksExternalParameterFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'exit_link_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'external_parameter_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'exit_link_id' => 1,
			'external_parameter_id' => 1
		),
	);

}

<?php

/**
 * CompanyFoldersContentFixture
 *
 */
class CompanyFoldersContentFixture extends CakeTestFixture {

    /**
     * Fields
     *
     * @var array
     */
    public $fields = array(
        'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
        'company_folder_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
        'model' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 50, 'key' => 'index', 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
        'model_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
        'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
        'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
        'indexes' => array(
            'PRIMARY' => array('column' => 'id', 'unique' => 1),
            'company_folder_id' => array('column' => 'company_folder_id', 'unique' => 0),
            'model' => array('column' => 'model', 'unique' => 0),
            'foreign_key' => array('column' => 'model_id', 'unique' => 0)
        ),
        'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
    );

    /**
     * Records
     *
     * @var array
     */
    public $records = array(
        array(
            'id' => 1,
            'company_folder_id' => 1,
            'model' => 'Lorem ipsum dolor sit amet',
            'model_id' => 1,
            'created' => '2014-11-28 12:37:17',
            'modified' => '2014-11-28 12:37:17'
        ),
    );

}

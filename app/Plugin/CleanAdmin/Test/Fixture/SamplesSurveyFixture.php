<?php
/**
 * SamplesSurveyFixture
 *
 */
class SamplesSurveyFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'sample_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'survey_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'sample_id' => 1,
			'survey_id' => 1,
			'modified' => '2014-06-24 08:37:09',
			'created' => '2014-06-24 08:37:09'
		),
	);

}

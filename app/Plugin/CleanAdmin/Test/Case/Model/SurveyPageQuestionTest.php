<?php
App::uses('SurveyPageQuestion', 'CleanAdmin.Model');

/**
 * SurveyPageQuestion Test Case
 *
 */
class SurveyPageQuestionTest extends CakeTestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->SurveyPageQuestion = ClassRegistry::init('CleanAdmin.SurveyPageQuestion');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->SurveyPageQuestion);

		parent::tearDown();
	}

}

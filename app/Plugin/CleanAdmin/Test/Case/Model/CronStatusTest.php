<?php
App::uses('CronStatus', 'CleanAdmin.Model');

/**
 * CronStatus Test Case
 *
 */
class CronStatusTest extends CakeTestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->CronStatus = ClassRegistry::init('CleanAdmin.CronStatus');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->CronStatus);

		parent::tearDown();
	}

}

<?php
App::uses('CompanyAttributeSet', 'CleanAdmin.Model');

/**
 * CompanyAttributeSet Test Case
 *
 */
class CompanyAttributeSetTest extends CakeTestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->CompanyAttributeSet = ClassRegistry::init('CleanAdmin.CompanyAttributeSet');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->CompanyAttributeSet);

		parent::tearDown();
	}

}

<?php
App::uses('SurveyPage', 'CleanAdmin.Model');

/**
 * SurveyPage Test Case
 *
 */
class SurveyPageTest extends CakeTestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->SurveyPage = ClassRegistry::init('CleanAdmin.SurveyPage');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->SurveyPage);

		parent::tearDown();
	}

}

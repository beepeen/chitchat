<?php

App::uses('CompanyFoldersUser', 'CleanAdmin.Model');

/**
 * CompanyFoldersUser Test Case
 *
 */
class CompanyFoldersUserTest extends CakeTestCase {

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp() {
        parent::setUp();
        $this->CompanyFoldersUser = ClassRegistry::init('CleanAdmin.CompanyFoldersUser');

    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown() {
        unset($this->CompanyFoldersUser);

        parent::tearDown();

    }

}

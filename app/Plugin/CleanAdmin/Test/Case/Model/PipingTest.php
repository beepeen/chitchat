<?php
App::uses('Piping', 'CleanAdmin.Model');

/**
 * Piping Test Case
 *
 */
class PipingTest extends CakeTestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Piping = ClassRegistry::init('CleanAdmin.Piping');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Piping);

		parent::tearDown();
	}

}

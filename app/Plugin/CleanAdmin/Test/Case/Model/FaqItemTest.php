<?php
App::uses('FaqItem', 'CleanAdmin.Model');

/**
 * FaqItem Test Case
 *
 */
class FaqItemTest extends CakeTestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->FaqItem = ClassRegistry::init('CleanAdmin.FaqItem');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->FaqItem);

		parent::tearDown();
	}

}

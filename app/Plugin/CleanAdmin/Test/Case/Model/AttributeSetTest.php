<?php
App::uses('AttributeSet', 'CleanAdmin.Model');

/**
 * AttributeSet Test Case
 *
 */
class AttributeSetTest extends CakeTestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->AttributeSet = ClassRegistry::init('CleanAdmin.AttributeSet');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->AttributeSet);

		parent::tearDown();
	}

}

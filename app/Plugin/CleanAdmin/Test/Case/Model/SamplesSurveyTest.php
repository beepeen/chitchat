<?php
App::uses('SamplesSurvey', 'CleanAdmin.Model');

/**
 * SamplesSurvey Test Case
 *
 */
class SamplesSurveyTest extends CakeTestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->SamplesSurvey = ClassRegistry::init('CleanAdmin.SamplesSurvey');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->SamplesSurvey);

		parent::tearDown();
	}

}

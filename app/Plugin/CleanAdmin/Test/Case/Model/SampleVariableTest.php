<?php
App::uses('SampleVariable', 'CleanAdmin.Model');

/**
 * SampleVariable Test Case
 *
 */
class SampleVariableTest extends CakeTestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->SampleVariable = ClassRegistry::init('CleanAdmin.SampleVariable');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->SampleVariable);

		parent::tearDown();
	}

}

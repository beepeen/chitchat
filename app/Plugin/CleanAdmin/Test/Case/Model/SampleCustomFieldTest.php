<?php
App::uses('SampleCustomField', 'CleanAdmin.Model');

/**
 * SampleCustomField Test Case
 *
 */
class SampleCustomFieldTest extends CakeTestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->SampleCustomField = ClassRegistry::init('CleanAdmin.SampleCustomField');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->SampleCustomField);

		parent::tearDown();
	}

}

<?php

App::uses('CompanyFoldersCompanyGroup', 'CleanAdmin.Model');

/**
 * CompanyFoldersCompanyGroup Test Case
 *
 */
class CompanyFoldersCompanyGroupTest extends CakeTestCase {

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp() {
        parent::setUp();
        $this->CompanyFoldersCompanyGroup = ClassRegistry::init('CleanAdmin.CompanyFoldersCompanyGroup');

    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown() {
        unset($this->CompanyFoldersCompanyGroup);

        parent::tearDown();

    }

}

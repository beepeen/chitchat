<?php
App::uses('ExitLinksExternalParameter', 'CleanAdmin.Model');

/**
 * ExitLinksExternalParameter Test Case
 *
 */
class ExitLinksExternalParameterTest extends CakeTestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ExitLinksExternalParameter = ClassRegistry::init('CleanAdmin.ExitLinksExternalParameter');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ExitLinksExternalParameter);

		parent::tearDown();
	}

}

<?php
App::uses('SampleSubscription', 'CleanAdmin.Model');

/**
 * SampleSubscription Test Case
 *
 */
class SampleSubscriptionTest extends CakeTestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->SampleSubscription = ClassRegistry::init('CleanAdmin.SampleSubscription');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->SampleSubscription);

		parent::tearDown();
	}

}

<?php
App::uses('Trigger', 'CleanAdmin.Model');

/**
 * Trigger Test Case
 *
 */
class TriggerTest extends CakeTestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Trigger = ClassRegistry::init('CleanAdmin.Trigger');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Trigger);

		parent::tearDown();
	}

}

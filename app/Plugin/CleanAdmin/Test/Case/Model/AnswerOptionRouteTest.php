<?php
App::uses('AnswerOptionRoute', 'CleanAdmin.Model');

/**
 * AnswerOptionRoute Test Case
 *
 */
class AnswerOptionRouteTest extends CakeTestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->AnswerOptionRoute = ClassRegistry::init('CleanAdmin.AnswerOptionRoute');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->AnswerOptionRoute);

		parent::tearDown();
	}

}

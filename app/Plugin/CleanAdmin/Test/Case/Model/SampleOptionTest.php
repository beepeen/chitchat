<?php
App::uses('SampleOption', 'CleanAdmin.Model');

/**
 * SampleOption Test Case
 *
 */
class SampleOptionTest extends CakeTestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->SampleOption = ClassRegistry::init('CleanAdmin.SampleOption');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->SampleOption);

		parent::tearDown();
	}

}

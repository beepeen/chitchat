<?php
App::uses('CompanyGroup', 'CleanAdmin.Model');

/**
 * CompanyGroup Test Case
 *
 */
class CompanyGroupTest extends CakeTestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->CompanyGroup = ClassRegistry::init('CleanAdmin.CompanyGroup');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->CompanyGroup);

		parent::tearDown();
	}

}

<?php
App::uses('ResponseDetail', 'CleanAdmin.Model');

/**
 * ResponseDetail Test Case
 *
 */
class ResponseDetailTest extends CakeTestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->ResponseDetail = ClassRegistry::init('CleanAdmin.ResponseDetail');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ResponseDetail);

		parent::tearDown();
	}

}

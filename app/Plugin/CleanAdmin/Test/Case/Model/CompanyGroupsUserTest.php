<?php
App::uses('CompanyGroupsUser', 'CleanAdmin.Model');

/**
 * CompanyGroupsUser Test Case
 *
 */
class CompanyGroupsUserTest extends CakeTestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->CompanyGroupsUser = ClassRegistry::init('CleanAdmin.CompanyGroupsUser');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->CompanyGroupsUser);

		parent::tearDown();
	}

}

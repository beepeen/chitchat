<?php
App::uses('SampleUploadLog', 'CleanAdmin.Model');

/**
 * SampleUploadLog Test Case
 *
 */
class SampleUploadLogTest extends CakeTestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->SampleUploadLog = ClassRegistry::init('CleanAdmin.SampleUploadLog');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->SampleUploadLog);

		parent::tearDown();
	}

}

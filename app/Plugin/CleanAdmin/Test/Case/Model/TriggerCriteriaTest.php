<?php
App::uses('TriggerCriteria', 'CleanAdmin.Model');

/**
 * TriggerCriteria Test Case
 *
 */
class TriggerCriteriaTest extends CakeTestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->TriggerCriteria = ClassRegistry::init('CleanAdmin.TriggerCriteria');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->TriggerCriteria);

		parent::tearDown();
	}

}

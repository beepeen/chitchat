<?php
App::uses('MenuItem', 'CleanAdmin.Model');

/**
 * MenuItem Test Case
 *
 */
class MenuItemTest extends CakeTestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->MenuItem = ClassRegistry::init('CleanAdmin.MenuItem');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->MenuItem);

		parent::tearDown();
	}

}

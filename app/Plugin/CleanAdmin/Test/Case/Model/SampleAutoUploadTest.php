<?php
App::uses('SampleAutoUpload', 'CleanAdmin.Model');

/**
 * SampleAutoUpload Test Case
 *
 */
class SampleAutoUploadTest extends CakeTestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->SampleAutoUpload = ClassRegistry::init('CleanAdmin.SampleAutoUpload');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->SampleAutoUpload);

		parent::tearDown();
	}

}

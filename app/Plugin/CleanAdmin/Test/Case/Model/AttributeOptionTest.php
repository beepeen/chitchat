<?php
App::uses('AttributeOption', 'CleanAdmin.Model');

/**
 * AttributeOption Test Case
 *
 */
class AttributeOptionTest extends CakeTestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->AttributeOption = ClassRegistry::init('CleanAdmin.AttributeOption');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->AttributeOption);

		parent::tearDown();
	}

}

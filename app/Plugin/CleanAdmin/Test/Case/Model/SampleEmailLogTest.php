<?php
App::uses('SampleEmailLog', 'CleanAdmin.Model');

/**
 * SampleEmailLog Test Case
 *
 */
class SampleEmailLogTest extends CakeTestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->SampleEmailLog = ClassRegistry::init('CleanAdmin.SampleEmailLog');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->SampleEmailLog);

		parent::tearDown();
	}

}

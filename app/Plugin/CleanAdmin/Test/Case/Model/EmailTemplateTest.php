<?php
App::uses('EmailTemplate', 'CleanAdmin.Model');

/**
 * EmailTemplate Test Case
 *
 */
class EmailTemplateTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		''
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->EmailTemplate = ClassRegistry::init('CleanAdmin.EmailTemplate');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->EmailTemplate);

		parent::tearDown();
	}

}

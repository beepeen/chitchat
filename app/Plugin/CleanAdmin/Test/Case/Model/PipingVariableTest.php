<?php
App::uses('PipingVariable', 'CleanAdmin.Model');

/**
 * PipingVariable Test Case
 *
 */
class PipingVariableTest extends CakeTestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->PipingVariable = ClassRegistry::init('CleanAdmin.PipingVariable');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->PipingVariable);

		parent::tearDown();
	}

}

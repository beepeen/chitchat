<?php

App::uses('CompanyFolderLog', 'CleanAdmin.Model');

/**
 * CompanyFolderLog Test Case
 *
 */
class CompanyFolderLogTest extends CakeTestCase {

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp() {
        parent::setUp();
        $this->CompanyFolderLog = ClassRegistry::init('CleanAdmin.CompanyFolderLog');

    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown() {
        unset($this->CompanyFolderLog);

        parent::tearDown();

    }

}

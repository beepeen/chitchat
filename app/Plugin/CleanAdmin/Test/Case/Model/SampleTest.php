<?php
App::uses('Sample', 'CleanAdmin.Model');

/**
 * Sample Test Case
 *
 */
class SampleTest extends CakeTestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Sample = ClassRegistry::init('CleanAdmin.Sample');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Sample);

		parent::tearDown();
	}

}

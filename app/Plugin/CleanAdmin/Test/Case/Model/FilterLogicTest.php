<?php
App::uses('FilterLogic', 'CleanAdmin.Model');

/**
 * FilterLogic Test Case
 *
 */
class FilterLogicTest extends CakeTestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->FilterLogic = ClassRegistry::init('CleanAdmin.FilterLogic');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->FilterLogic);

		parent::tearDown();
	}

}

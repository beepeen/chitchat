<?php

App::uses('CompanyFolder', 'CleanAdmin.Model');

/**
 * CompanyFolder Test Case
 *
 */
class CompanyFolderTest extends CakeTestCase {

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp() {
        parent::setUp();
        $this->CompanyFolder = ClassRegistry::init('CleanAdmin.CompanyFolder');

    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown() {
        unset($this->CompanyFolder);

        parent::tearDown();

    }

}

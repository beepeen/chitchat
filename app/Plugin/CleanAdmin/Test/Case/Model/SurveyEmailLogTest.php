<?php
App::uses('SurveyEmailLog', 'CleanAdmin.Model');

/**
 * SurveyEmailLog Test Case
 *
 */
class SurveyEmailLogTest extends CakeTestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->SurveyEmailLog = ClassRegistry::init('CleanAdmin.SurveyEmailLog');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->SurveyEmailLog);

		parent::tearDown();
	}

}

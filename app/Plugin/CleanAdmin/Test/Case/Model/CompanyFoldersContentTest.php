<?php

App::uses('CompanyFoldersContent', 'CleanAdmin.Model');

/**
 * CompanyFoldersContent Test Case
 *
 */
class CompanyFoldersContentTest extends CakeTestCase {

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp() {
        parent::setUp();
        $this->CompanyFoldersContent = ClassRegistry::init('CleanAdmin.CompanyFoldersContent');

    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown() {
        unset($this->CompanyFoldersContent);

        parent::tearDown();

    }

}

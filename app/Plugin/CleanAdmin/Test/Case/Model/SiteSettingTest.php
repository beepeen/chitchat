<?php
App::uses('SiteSetting', 'CleanAdmin.Model');

/**
 * SiteSetting Test Case
 *
 */
class SiteSettingTest extends CakeTestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->SiteSetting = ClassRegistry::init('CleanAdmin.SiteSetting');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->SiteSetting);

		parent::tearDown();
	}

}

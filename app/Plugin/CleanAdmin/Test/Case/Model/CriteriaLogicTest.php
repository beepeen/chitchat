<?php
App::uses('CriteriaLogic', 'CleanAdmin.Model');

/**
 * CriteriaLogic Test Case
 *
 */
class CriteriaLogicTest extends CakeTestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->CriteriaLogic = ClassRegistry::init('CleanAdmin.CriteriaLogic');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->CriteriaLogic);

		parent::tearDown();
	}

}

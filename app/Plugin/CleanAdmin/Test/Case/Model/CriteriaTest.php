<?php
App::uses('Criteria', 'CleanAdmin.Model');

/**
 * Criteria Test Case
 *
 */
class CriteriaTest extends CakeTestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Criteria = ClassRegistry::init('CleanAdmin.Criteria');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Criteria);

		parent::tearDown();
	}

}

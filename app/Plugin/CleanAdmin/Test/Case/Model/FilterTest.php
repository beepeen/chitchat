<?php
App::uses('Filter', 'CleanAdmin.Model');

/**
 * Filter Test Case
 *
 */
class FilterTest extends CakeTestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Filter = ClassRegistry::init('CleanAdmin.Filter');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Filter);

		parent::tearDown();
	}

}

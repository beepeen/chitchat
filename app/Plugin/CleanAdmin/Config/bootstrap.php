<?php
/**
 * CleanAdmin CakePHP Plugin
 *
 * Copyright 2014, BeepeEn ManandHar
 *                 Kathmandu, Nepal
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @Copyright 2014, BeepeEn ManandHar
 * @package   plugins.cakeadmin.config.schema
 * @license   MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

define("LIMIT", 10);

?>

<?php

App::uses('AppController', 'Controller');

class CleanAdminAppController extends AppController {

    /**
     * Function to generate search conditions on basis of fields given
     * @param array $fields
     * @return array $conditions
     */
    protected function __getSearchConditions($fields = array()) {
        $searchConditions = array();
        if ($this->request->is('post') && isset($this->request->data['searchKey'])) {
            $searchKey = $this->request->data['searchKey'];
            if (!empty($fields)) {
                foreach ($fields as $field) {
                    $searchConditions[$field . ' LIKE'] = '%' . $searchKey . '%';
                }
            }
        }
        
        return array('OR' => $searchConditions);
    }

}

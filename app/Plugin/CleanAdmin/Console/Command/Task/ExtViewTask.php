<?php

/**
 * The ViewTask handles creating and updating views files.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @Copyright 2014, BeepeEn ManandHar
 * @package   plugins.CleanAdmin
 * @license   MIT License (http://www.opensource.org/licenses/mit-license.php)

  App::uses('ViewTask', 'Console/Command/Task');

  /**
 * Task class for creating and updating model files.
 *
 * @package       Cake.Console.Command.Task
 */
class ExtViewTask extends ViewTask {

    public $name = 'View';
    public $scaffoldActions = array('index', 'add', 'edit', 'form');

    public function execute() {
        parent::execute();
    }

    /**
     * get the option parser.
     *
     * @return void
     */
    public function getOptionParser() {
        $parser = parent::getOptionParser();
        return
                        $parser
                        ->addOption('plugin', array(
                            'short' => 'l',
                            'help' => __d('cake_console', 'Plugin.')
                        ))

        ;
    }

    public function bake($action, $content = '') {
        if ($content === true) {
            $content = $this->getContent($action);
        }
        if (empty($content)) {
            return false;
        }
        $this->out("\n" . __d('cake_console', 'Baking `%s` view file...', $action), 1, Shell::QUIET);

        $path = $this->getPath();
        $filename = $path . 'Themed' . DS . 'Admin' . DS . $this->controllerName . DS . Inflector::underscore($action) . '.ctp';
        $this->out("\n" . __d('cake_console', $filename), 1, Shell::QUIET);
        return $this->createFile($filename, $content);
    }
    
    public function getTemplate($action) {
		if ($action != $this->template && in_array($action, $this->noTemplateActions)) {
			return false;
		}
		if (!empty($this->template) && $action != $this->template) {
			return $this->template;
		}
		$themePath = $this->Template->getThemePath();
		if (file_exists($themePath . 'views' . DS . $action . '.ctp')) {
			return $action;
		}
		$template = $action;
		$prefixes = Configure::read('Routing.prefixes');
		foreach ((array)$prefixes as $prefix) {
			if (strpos($template, $prefix) !== false) {
				$template = str_replace($prefix . '_', '', $template);
			}
		}
		if (in_array($template, array('add', 'edit'))) {
			$template = 'add';
		} elseif (preg_match('@(_add|_edit)$@', $template)) {
			$template = str_replace(array('_add', '_edit'), '_add', $template);
		}
		return $template;
	}

}
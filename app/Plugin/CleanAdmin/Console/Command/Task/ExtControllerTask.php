<?php

/**
 * The ControllerTask handles creating and updating controller files.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2011, Cake Software Foundation, Inc.
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         CakePHP(tm) v 1.2
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('ControllerTask', 'Console/Command/Task');

/**
 * Task class for creating and updating controller files.
 *
 * @package       Cake.Console.Command.Task
 */
class ExtControllerTask extends ControllerTask {

    public $name = 'Controller';

    public function execute() {
        parent::execute();
    }

    /**
     * get the option parser.
     *
     * @return void
     */
    public function getOptionParser() {
        $parser = parent::getOptionParser();
        return $parser
                        ->addOption('plugin', array(
                            'short' => 'l',
                            'help' => __d('cake_console', 'Plugin.')
        ));
    }

    /**
     * Gets the path for output. Checks the plugin property
     * and returns the correct path.
     *
     * @return string Path to output.
     */
    public function getPath() {
        $path = $this->path;

        return $path;
    }

    
    /**
     * Bake scaffold actions
     *
     * @param string $controllerName Controller name
     * @param string $admin Admin route to use
     * @param boolean $wannaUseSession Set to true to use sessions, false otherwise
     * @return string Baked actions
     */
    public function bakeActions($controllerName, $admin = null, $wannaUseSession = true) {
        $currentModelName = $modelImport = $this->_modelName($controllerName);

        App::uses($modelImport, 'Model');
        if (!class_exists($modelImport)) {
            $this->err(__d('cake_console', 'You must have a model for this class to build basic methods. Please try again.'));
            return $this->_stop();
        }

        $modelObj = ClassRegistry::init($currentModelName);
        $controllerPath = $this->_controllerPath($controllerName);
        $pluralName = $this->_pluralName($currentModelName);
        $singularName = Inflector::variable($currentModelName);
        $singularHumanName = $this->_singularHumanName($controllerName);
        $pluralHumanName = $this->_pluralName($controllerName);
        $displayField = $modelObj->displayField;
        $primaryKey = $modelObj->primaryKey;

        $this->Template->set(compact(
                        'plugin', 'admin', 'controllerPath', 'pluralName', 'singularName', 'singularHumanName', 'pluralHumanName', 'modelObj', 'wannaUseSession', 'currentModelName', 'displayField', 'primaryKey'
        ));
        $actions = $this->Template->generate('actions', 'controller_actions');
        return $actions;
    }

}

<?php
/**
 *
 * PHP 5
 *  
 * CakeAdmin CakePHP Plugin
 *
 * Copyright 2014, BeepeEn ManandHar
 *                 Kathmandu, Nepal
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @Copyright 2014, BeepeEn ManandHar
 * @package   plugins.cakeadmin.config.schema
 * @license   MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<?php
echo "<?php 
/**
 *
 * PHP 5
 *  
 * CakeAdmin CakePHP Plugin
 *
 * Copyright 2014, BeepeEn ManandHar
 *                 Kathmandu, Nepal
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @Copyright 2014, BeepeEn ManandHar
 * @package   plugins.cakeadmin.config.schema
 * @license   MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
 ?>"
?>
<!--Begin <?php echo $pluralVar; ?> tables-->
<div class="row <?php echo $pluralVar; ?>">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header">
                <i class="glyphicon glyphicon-list-alt"></i>
                <h3 class="box-title"><?php echo "<?php echo __('{$pluralHumanName}'); ?>"; ?> <?php echo __('List') ?></h3>      
                <div class="box-tools">
                    <form method="post" action="">
                        <div class="input-group">
                            <?php
                           
                            echo "<?php\n";
                            echo "\t\techo \$this->Form->input('searchKey', array(
                             'placeholder' => __('" . ucwords('Search') . "'),
                              'class' => 'form-control input-sm searchbox pull-right',
                              'label'=>false,
                             ));\n";
                            echo "\t?>\n";
                            
                            ?>
                            <div class="input-group-btn">
                                <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div><!-- /.box-header -->
            <!-- Flash Msg -->
            <?php echo "<?php echo \$this->Session->flash(); ?>"; ?>
            <?php echo "\n"; ?>
            <?php echo "<?php echo \$this->Session->flash('auth'); ?>"; ?>
            <?php echo "\n"; ?>
            <!--/Flash Msg -->
            <div class="box-body table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <?php $count = 0 ?>
                            <?php foreach ($fields as $field): ?>
                                <?php if ($count == 0) { ?>
                                    <th><?php echo "<?php echo \$this->Paginator->sort('{$field}'); ?>"; ?></th>
                                <?php } else { ?>
                                    <th><?php echo "<?php echo \$this->Paginator->sort('{$field}'); ?>"; ?></th>
                                    <?php
                                }
                                $count++;
                                ?>
                            <?php endforeach; ?>
                            <th class="actions"><?php echo "<?php echo __('Actions'); ?>"; ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        echo "<?php foreach (\${$pluralVar} as \${$singularVar}): ?>\n";
                        echo "\t<tr>\n";
                        foreach ($fields as $field) {
                            $isKey = false;
                            if (!empty($associations['belongsTo'])) {
                                foreach ($associations['belongsTo'] as $alias => $details) {
                                    if ($field === $details['foreignKey']) {
                                        $isKey = true;
                                        echo "\t\t<td><strong>\n\t\t\t<?php echo \$this->AclHtml->link(\${$singularVar}['{$alias}']['{$details['displayField']}'], array('controller' => '{$details['controller']}', 'action' => 'edit', \${$singularVar}['{$alias}']['{$details['primaryKey']}'])); ?>\n\t\t</strong></td>\n";
                                        break;
                                    }
                                }
                            }
                            if ($isKey !== true) {
                                echo "\t\t<td><?php echo h(\${$singularVar}['{$modelClass}']['{$field}']); ?>&nbsp;</td>\n";
                            }
                        }

                        echo "\t\t<td class=\"actions\">\n";
                        echo "\t\t\t<?php echo \$this->AclHtml->link('<i class=\"glyphicon glyphicon-pencil\"></i>', array('action' => 'edit', \${$singularVar}['{$modelClass}']['{$primaryKey}']),array('escape' => false)); ?>\n";
                        echo "&nbsp;";
                        echo "\t\t\t<?php echo \$this->AclHtml->postLink('<i class=\"glyphicon glyphicon-trash\"></i>', array('action' => 'delete', \${$singularVar}['{$modelClass}']['{$primaryKey}']),  array('escape' => false), __('Are you sure you want to delete # %s?', \${$singularVar}['{$modelClass}']['{$primaryKey}'])); ?>\n";
                        echo "\t\t</td>\n";
                        echo "\t</tr>\n";

                        echo "<?php endforeach; ?>\n";
                        ?>

                    </tbody>
                    <?php/*<tfoot>
                        <tr>
                            <?php $count = 0 ?>
                            <?php foreach ($fields as $field): ?>
                                <?php if ($count == 0) { ?>
                                    <th><?php echo "<?php echo \$this->Paginator->sort('{$field}'); ?>"; ?></th>
                                <?php } else { ?>
                                    <th><?php echo "<?php echo \$this->Paginator->sort('{$field}'); ?>"; ?></th>
                                    <?php
                                }
                                $count++;
                                ?>
                            <?php endforeach; ?>
                            <th class="actions"><?php echo "<?php echo __('Actions'); ?>"; ?></th>
                        </tr>
                    </tfoot>*/?>
                </table>
                <?php echo "<?php echo \$this->element('pagination'); ?>\n"; ?>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div><!-- /.row -->
<!--End <?php echo $pluralVar; ?> div-->


<?php
/**
 *
 * PHP 5
 *  
 * CakeAdmin CakePHP Plugin
 *
 * Copyright 2014, BeepeEn ManandHar
 *                 Kathmandu, Nepal
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @Copyright 2014, BeepeEn ManandHar
 * @package   plugins.cakeadmin.config.schema
 * @license   MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<?php
echo "<?php 
/**
 *
 * PHP 5
 *  
 * CakeAdmin CakePHP Plugin
 *
 * Copyright 2014, BeepeEn ManandHar
 *                 Kathmandu, Nepal
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @Copyright 2014, BeepeEn ManandHar
 * @package   plugins.cakeadmin.config.schema
 * @license   MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
 ?>"
?>
<!-- <?php echo $pluralVar; ?> add/edit elements -->
<div class="col-md-12 ">
    <div class="box box-primary <?php echo $pluralVar; ?>">
        <div class="box-header">
            <h3 class="box-title"><?php printf("<?php echo __('%s %s'); ?>", Inflector::humanize($action), $singularHumanName); ?></h3>
          
            <?php if (strpos($action, 'edit') == true): ?>
                <div class="pull-right box-tools">
           
                        <?php
                        $done = array();
                        foreach ($associations as $type => $data) {
                            foreach ($data as $alias => $details) {
                                if ($details['controller'] != $this->name && !in_array($details['controller'], $done)) {
                                    echo "\t\t<?php echo \$this->AclHtml->link('<i class=\"fa fa-list\"></i>'.__('" . Inflector::humanize($details['controller']) . "'), array('controller' => '{$details['controller']}', 'action' => 'index'),array('class' => 'btn btn-app','escape' => false)); ?> \n";
                                    echo "\t\t<?php echo \$this->AclHtml->link('<i class=\"fa fa-plus\"></i>'.__('Add " . Inflector::humanize(Inflector::underscore($alias)) . "'), array('controller' => '{$details['controller']}', 'action' => 'add'),array('class' => 'btn btn-app','escape' => false)); ?> \n";
                                    $done[] = $details['controller'];
                                }
                            }
                        }
                        ?>
                </div>
            <?php endif; ?>
        </div><!-- /.box-header -->

        <!-- form start -->
        <?php echo "<?php\techo \$this->element('../" . ucwords($pluralVar) . "/admin_form')\t?>";  echo "\n"; ?>
        <!-- form end -->
    </div><!-- /.box -->
</div>
<!-- End <?php echo $pluralVar; ?> add/edit elements -->




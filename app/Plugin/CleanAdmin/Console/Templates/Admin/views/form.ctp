<?php
/**
 *
 * PHP 5
 *  
 * CakeAdmin CakePHP Plugin
 *
 * Copyright 2014, BeepeEn ManandHar
 *                 Kathmandu, Nepal
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @Copyright 2014, BeepeEn ManandHar
 * @package   plugins.cakeadmin.config.schema
 * @license   MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
 <?php
echo "<?php 
/**
 *
 * PHP 5
 *  
 * CakeAdmin CakePHP Plugin
 *
 * Copyright 2014, BeepeEn ManandHar
 *                 Kathmandu, Nepal
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @Copyright 2014, BeepeEn ManandHar
 * @package   plugins.cakeadmin.config.schema
 * @license   MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
 ?>"
?>
<!-- <?php echo $pluralVar; ?> form elements -->

    <?php echo "<?php echo \$this->element('validate_message'); ?>" ?>
        <!-- form start -->
        <?php echo "<?php echo \$this->Form->create('{$modelClass}',array('inputDefaults' => array('label' => false, 'div' => false))); ?>\n"; ?>
        <div class="box-body">
            <?php
            foreach ($fields as $field) {
                if (!in_array($field, array('created', 'modified', 'updated'))) {
                    if ($field == 'id') {
                        echo "\t<?php\tif(\$this->action == 'admin_edit'){\t\n";
                        echo "\t\t\techo \$this->Form->input('{$field}');\t\n}?>\n";
                      
                    } else {
                        ?>
            <div class="form-group">
                <label for="<?php echo $field ?>"><?php echo "<?php\techo __('" . Inflector::humanize($field) . "')\t?>" ?></label>
                <?php
                echo "<?php\n";
                echo "\t\techo \$this->Form->input('{$field}', array(
                'placeholder' => __('" . Inflector::humanize($field) . "'),
                'class' => 'form-control'
                ));\n";
                echo "\t?>\n";
                ?>
            </div><!-- /.box-body -->
                        <?php
                    }
                }
            }
            if (!empty($associations['hasAndBelongsToMany'])) {
                foreach ($associations['hasAndBelongsToMany'] as $assocName => $assocData) {
                    echo "\t\techo \$this->Form->input('{$assocName}');\n";
                }
            }
            ?>
        </div><!-- /.box-body -->

        <div class="box-footer">
            <input type="submit" value="<?php echo "<?php echo __('Save'); ?>"; ?>" class="btn btn-primary">
            <?php echo "\t<?php\techo \$this->Html->link(__('Cancel'), array('action' => 'index'),array('class' => 'btn btn-default'));\t?>\n"; ?>
        </div>
        <?php echo "<?php echo \$this->Form->end(); ?>\n"; ?>
<!-- End <?php echo $pluralVar; ?> form elements -->





<?php
/**
 * Bake Template for Controller action generation.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Console.Templates.default.actions
 * @since         CakePHP(tm) v 1.3
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>

/**
* <?php echo $admin ?>index method
*
* @return void
*/
public function <?php echo $admin ?>index() {
$searchFields = array(); //fields to b searched
$searchConditions = $this->__getSearchConditions($searchFields);
$this-><?php echo $currentModelName ?>->recursive = 0;
$this->Paginator->settings = array(
'conditions' => array(
$searchConditions
),
'limit' => LIMIT
);
$<?php echo $pluralName ?> = $this->Paginator->paginate();
$this->set(compact('<?php echo $pluralName ?>'));
}

/**
* <?php echo $admin ?>view method
*
* @throws NotFoundException
* @param string $id
* @return void
*/
public function <?php echo $admin ?>view($id = null) {
if (!$this-><?php echo $currentModelName; ?>->exists($id)) {
throw new NotFoundException(__('Invalid <?php echo strtolower($singularHumanName); ?>'));
}
$options = array('conditions' => array('<?php echo $currentModelName; ?>.' . $this-><?php echo $currentModelName; ?>->primaryKey => $id));
$<?php echo $singularName; ?> = $this-><?php echo $currentModelName; ?>->find('first', $options);
$this->request->data = $<?php echo $singularName; ?>;
$this->set(compact('<?php echo $singularName; ?>'));
}

<?php $compact = array(); ?>
/**
* <?php echo $admin ?>add method
*
* @return void
*/
public function <?php echo $admin ?>add() {
if ($this->request->is('post')) {
$this-><?php echo $currentModelName; ?>->create();
if ($this-><?php echo $currentModelName; ?>->save($this->request->data)) {
<?php if ($wannaUseSession): ?>
    $this->Session->setFlash(__('The <?php echo strtolower($singularHumanName); ?> has been saved.'), 'success_message');
    return $this->redirect(array('action' => 'index'));
    } else {
    $errors = $this-><?php echo $currentModelName; ?>->validationErrors;
    $this->set('errors', $errors);
    $this->Session->setFlash(__('The <?php echo strtolower($singularHumanName); ?> could not be saved. Please, try again.'), 'error_message');
<?php else: ?>
    return $this->flash(__('The <?php echo strtolower($singularHumanName); ?> has been saved.'), 'success_message', array('action' => 'index'));
<?php endif; ?>
}
}
<?php
foreach (array('belongsTo', 'hasAndBelongsToMany') as $assoc):
    foreach ($modelObj->{$assoc} as $associationName => $relation):
        if (!empty($associationName)):
            $otherModelName = $this->_modelName($associationName);
            $otherPluralName = $this->_pluralName($associationName);
            echo "\t\t\${$otherPluralName} = \$this->{$currentModelName}->{$otherModelName}->find('list');\n";
            $compact[] = "'{$otherPluralName}'";
        endif;
    endforeach;
endforeach;
if (!empty($compact)):
    echo "\t\t\$this->set(compact(" . join(', ', $compact) . "));\n";
endif;
?>
}

<?php $compact = array(); ?>
/**
* <?php echo $admin ?>edit method
*
* @throws NotFoundException
* @param string $id
* @return void
*/
public function <?php echo $admin; ?>edit($id = null) {
if (!$this-><?php echo $currentModelName; ?>->exists($id)) {
throw new NotFoundException(__('Invalid <?php echo strtolower($singularHumanName); ?>'));
}
if ($this->request->is(array('post', 'put'))) {
if ($this-><?php echo $currentModelName; ?>->save($this->request->data)) {
<?php if ($wannaUseSession): ?>
    $this->Session->setFlash(__('The <?php echo strtolower($singularHumanName); ?> has been saved.'), 'success_message');
    return $this->redirect(array('action' => 'index'));
    } else {
    $errors = $this-><?php echo $currentModelName; ?>->validationErrors;
    $this->set('errors', $errors);
    $this->Session->setFlash(__('The <?php echo strtolower($singularHumanName); ?> could not be saved. Please, try again.'), 'error_message');
<?php else: ?>
    return $this->flash(__('The <?php echo strtolower($singularHumanName); ?> has been saved.'), 'success_message', array('action' => 'index'));
<?php endif; ?>
}
} else {
$options = array('conditions' => array('<?php echo $currentModelName; ?>.' . $this-><?php echo $currentModelName; ?>->primaryKey => $id));
$this->request->data = $this-><?php echo $currentModelName; ?>->find('first', $options);
}
<?php
foreach (array('belongsTo', 'hasAndBelongsToMany') as $assoc):
    foreach ($modelObj->{$assoc} as $associationName => $relation):
        if (!empty($associationName)):
            $otherModelName = $this->_modelName($associationName);
            $otherPluralName = $this->_pluralName($associationName);
            echo "\t\t\${$otherPluralName} = \$this->{$currentModelName}->{$otherModelName}->find('list');\n";
            $compact[] = "'{$otherPluralName}'";
        endif;
    endforeach;
endforeach;
if (!empty($compact)):
    echo "\t\t\$this->set(compact(" . join(', ', $compact) . "));\n";
endif;
?>
}

/**
* <?php echo $admin ?>delete method
*
* @throws NotFoundException
* @param string $id
* @return void
*/
public function <?php echo $admin; ?>delete($id = null) {
$this-><?php echo $currentModelName; ?>->id = $id;
if (!$this-><?php echo $currentModelName; ?>->exists()) {
throw new NotFoundException(__('Invalid <?php echo strtolower($singularHumanName); ?>'));
}
$this->request->onlyAllow('post', 'delete');
if ($this-><?php echo $currentModelName; ?>->delete()) {
<?php if ($wannaUseSession): ?>
    $this->Session->setFlash(__('The <?php echo strtolower($singularHumanName); ?> has been deleted.'), 'success_message');
    } else {
    $this->Session->setFlash(__('The <?php echo strtolower($singularHumanName); ?> could not be deleted. Please, try again.'), 'error_message');
    }
    return $this->redirect(array('action' => 'index'));
<?php else: ?>
    return $this->flash(__('The <?php echo strtolower($singularHumanName); ?> has been deleted.'), 'success_message', array('action' => 'index'));
    } else {
    return $this->flash(__('The <?php echo strtolower($singularHumanName); ?> could not be deleted. Please, try again.'), 'error_message', array('action' => 'index'));
    }
<?php endif; ?>
}

<?php if ($currentModelName == 'User'): ?>   
    /**
    * <?php echo $admin ?>login method
    *
    * @return void
    */
    public function <?php echo $admin; ?>login() {
    if ($this->Session->read('Auth.Admin')) {
    $this->Session->setFlash('You are logged in!');
    return $this->redirect('/admin');
    }
    if ($this->request->is('post')) {
    if ($this->Auth->login()) {
    return $this->redirect($this->Auth->redirect());
    }
    $this->Session->setFlash(__('Your username or password was incorrect.'), 'error_message');
    }
    $this->layout = 'login';
    }


    /**
    * Admin logout
    * @return type
    */
    public function <?php echo $admin; ?>logout() {

    $this->Session->setFlash('Good-Bye', 'success_message');
    $this->Session->delete('Alaxos.Acl.permissions');
    $this->redirect($this->Auth->logout());
    }

<?php endif; ?>
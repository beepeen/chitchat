var lang = new Array();

lang["Please enter the username."] = "Please enter your username.";
lang["Please enter the password."] = "Please enter your password.";

/**
 * javascript translate function
 * add the key words with translated values on above array and to translate strings to your site
 * @param string
 * @returns {*}
 */
function translate(string) {
    if (string) {
        if (lang && language == 'nld') {
            if (lang.hasOwnProperty(string)) {
                return lang[string];
            }
        }
        return string;
    }
}
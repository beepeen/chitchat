<?php
/**
 * This file is loaded automatically by the app/webroot/index.php file after core.php
 *
 * This file should load/create any application wide configuration settings, such as
 * Caching, Logging, loading additional configuration files.
 *
 * You should also use this file to include any files that provide global functions/constants
 * that your application uses.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.10.8.2117
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

// Setup a 'default' cache configuration for use in the application.
Cache::config('default', array('engine' => 'File'));

/**
 * The settings below can be used to set additional paths to models, views and controllers.
 *
 * App::build(array(
 *     'Model'                     => array('/path/to/models/', '/next/path/to/models/'),
 *     'Model/Behavior'            => array('/path/to/behaviors/', '/next/path/to/behaviors/'),
 *     'Model/Datasource'          => array('/path/to/datasources/', '/next/path/to/datasources/'),
 *     'Model/Datasource/Database' => array('/path/to/databases/', '/next/path/to/database/'),
 *     'Model/Datasource/Session'  => array('/path/to/sessions/', '/next/path/to/sessions/'),
 *     'Controller'                => array('/path/to/controllers/', '/next/path/to/controllers/'),
 *     'Controller/Component'      => array('/path/to/components/', '/next/path/to/components/'),
 *     'Controller/Component/Auth' => array('/path/to/auths/', '/next/path/to/auths/'),
 *     'Controller/Component/Acl'  => array('/path/to/acls/', '/next/path/to/acls/'),
 *     'View'                      => array('/path/to/views/', '/next/path/to/views/'),
 *     'View/Helper'               => array('/path/to/helpers/', '/next/path/to/helpers/'),
 *     'Console'                   => array('/path/to/consoles/', '/next/path/to/consoles/'),
 *     'Console/Command'           => array('/path/to/commands/', '/next/path/to/commands/'),
 *     'Console/Command/Task'      => array('/path/to/tasks/', '/next/path/to/tasks/'),
 *     'Lib'                       => array('/path/to/libs/', '/next/path/to/libs/'),
 *     'Locale'                    => array('/path/to/locales/', '/next/path/to/locales/'),
 *     'Vendor'                    => array('/path/to/vendors/', '/next/path/to/vendors/'),
 *     'Plugin'                    => array('/path/to/plugins/', '/next/path/to/plugins/'),
 * ));
 *
 */

/**
 * Custom Inflector rules can be set to correctly pluralize or singularize table, model, controller names or whatever other
 * string is passed to the inflection functions
 *
 * Inflector::rules('singular', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 * Inflector::rules('plural', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 *
 */

/**
 * Plugins need to be loaded manually, you can either load them one by one or all of them in a single call
 * Uncomment one of the lines below, as you need. Make sure you read the documentation on CakePlugin to use more
 * advanced ways of loading plugins
 *
 * CakePlugin::loadAll(); // Loads all plugins at once
 * CakePlugin::load('DebugKit'); //Loads a single plugin named DebugKit
 *
 */
CakePlugin::load('CleanAdmin', array('bootstrap' => true));
CakePlugin::load('Acl', array('bootstrap' => true));
CakePlugin::load('ZoneAcl', array('bootstrap' => true));
//CakePlugin::load('Migrations');
//CakePlugin::load('Upload');
//CakePlugin::load('PhpThumb');
//Configure::write('PhpThumb.thumbs_path','img/thumbs');

/**
 * You can attach event listeners to the request lifecycle as Dispatcher Filter. By default CakePHP bundles two filters:
 *
 * - AssetDispatcher filter will serve your asset files (css, images, js, etc) from your themes and plugins
 * - CacheDispatcher filter will read the Cache.check configure variable and try to serve cached content generated from controllers
 *
 * Feel free to remove or add filters as you see fit for your application. A few examples:
 *
 * Configure::write('Dispatcher.filters', array(
 *		'MyCacheFilter', //  will use MyCacheFilter class from the Routing/Filter package in your app.
 *		'MyCacheFilter' => array('prefix' => 'my_cache_'), //  will use MyCacheFilter class from the Routing/Filter package in your app with settings array.
 *		'MyPlugin.MyFilter', // will use MyFilter class from the Routing/Filter package in MyPlugin plugin.
 *		array('callable' => $aFunction, 'on' => 'before', 'priority' => 9), // A valid PHP callback type to be called on beforeDispatch
 *		array('callable' => $anotherMethod, 'on' => 'after'), // A valid PHP callback type to be called on afterDispatch
 *
 * ));
 */
Configure::write('Dispatcher.filters', array(
	'AssetDispatcher',
	'CacheDispatcher'
));

/**
 * Set App Url for use in shell
 */
if (php_sapi_name() == 'cli') {
    switch (SITE_TYPE) {
        case 'local':
            Configure::write('App.fullBaseUrl', 'http://localhost/chitchat');
            break;

        case 'demo':
            Configure::write('App.fullBaseUrl', 'http://demo.chitchat.com');
            break;

        case 'live':
            Configure::write('App.fullBaseUrl', 'http://live.chitchat.com');
            break;

        default:
            break;
    }
}

/**
 * Configures default file logging options
 */
App::uses('CakeLog', 'Log');
CakeLog::config('debug', array(
	'engine' => 'File',
	'types' => array('notice', 'info', 'debug'),
	'file' => 'debug',
    'mask' => 0666,
));
CakeLog::config('error', array(
	'engine' => 'File',
	'types' => array('warning', 'error', 'critical', 'alert', 'emergency'),
	'file' => 'error',
    'mask' => 0666,
));

/**
 * Cache setting for dashboard widget custom variable folders filter
 */

define('IV_SIZE', 8);
define('MCRYPT_ENCRYPTION_KEY', 'whs8d7f6hcb346251gds34ksd8fg644gd2b42n34j');


/**
 * Composer autoload
 */
App::import('Vendor', array('file' => 'autoload'));

/**
 * function to generate unique filename for images
 * @param string $filename
 * @return string
 */
function getUniqueFilename($filename) {
	$uniquename = uniqid();
	$fileext = explode(".", $filename);
	$mimeType = end($fileext);
	$filename = $uniquename . "." . $mimeType;
	return $filename;
}

/**
 * Mcrypt Blowfish 64 bit encryption Generic Implementation
 * @param $textToEncrypt
 * @return string
 */
function encryptString($textToEncrypt) {
    // pack key into binary string
    $key = pack('H*', MCRYPT_ENCRYPTION_KEY);

    // create a random IV to use with CBC encoding
    $iv = mcrypt_create_iv(IV_SIZE, MCRYPT_RAND);

    // creates a cipher text compatible with AES (Blowfish block size = 64)
    $cipherText = mcrypt_encrypt(MCRYPT_BLOWFISH, $key, $textToEncrypt, MCRYPT_MODE_CBC, $iv);

    // prepend the IV for it to be available for decryption
    $cipherText = $iv . $cipherText;

    // encode the resulting cipher text so it can be represented by a string
    $ciphertextBase64 = base64_encode($cipherText);

    return $ciphertextBase64;
}

/**
 * Mcrypt Blowfish 64 bit decryption Generic Implementation
 * @param $encryptedText
 * @return string
 */
function decryptString($encryptedText) {
	$cipherText = base64_decode($encryptedText);

    // pack key into binary string
    $key = pack('H*', MCRYPT_ENCRYPTION_KEY);

    // get the IV created during encryption
    $iv = substr($cipherText, 0, IV_SIZE);

    // get the cipher text
    $cipherText = substr($cipherText, IV_SIZE);
    $originalText = mcrypt_decrypt(MCRYPT_BLOWFISH, $key, $cipherText, MCRYPT_MODE_CBC, $iv);

    return rtrim($originalText, "\0");
}

spl_autoload_register(function ($class) {
    foreach (App::path('Vendor') as $base) {
        $path = $base . str_replace('\\', DS, $class) . '.php';
        if (file_exists($path)) {
            include $path;
            return;
        }
    }
});

/**
 * function to unset the variable already used and free up memory
 * @param $variable
 */
function unsetVariable(&$variable) {
	$variable = null;
	unset($variable);
}

/**
 * function to remove any unwanted encoded strings or tags
 * @param $string
 * @return mixed|string
 */
function getCleanText($string) {
    $string = str_replace("&nbsp;", " ", $string);
    $string = str_replace("&rsquo;", "'", $string);
    $string = htmlspecialchars_decode($string, ENT_QUOTES);
    $string = trim(strip_tags($string));
    return $string;
}

/**
 * getEmailToken format
 * @param $email
 * @return string
 */
function getEmailToken($email) {
    $emailToken = sha1(SAMPLE_SEED . $email, false);
    return $emailToken;
}

/**
 * clean email format
 * @param $email
 * @return mixed
 */
function getCleanEmail($email) {
    $email = filter_var(trim($email), FILTER_SANITIZE_EMAIL);
    return $email;
}

/**
 * utf8_encode_deep function to encode special chars for json encoding
 * @param $input
 */
function utf8_encode_deep(&$input) {
    if (is_string($input)) {
        $input = utf8_encode($input);
    } else if (is_array($input)) {
        foreach ($input as &$value) {
            utf8_encode_deep($value);
        }

        unset($value);
    } else if (is_object($input)) {
        $vars = array_keys(get_object_vars($input));

        foreach ($vars as $var) {
            utf8_encode_deep($input->$var);
        }
    }
}

/**
 * Custom arrayCombine function to combine arrays of different sizes
 * @param type array
 * @param type array
 * @return array
 */
function arrayCombine($array1, $array2) {
    if(count($array1)!=count($array2)) {
        $acount = count($array1);
        $bcount = count($array2);
        $size = ($acount > $bcount) ? $bcount : $acount;
        $array1 = array_slice($array1, 0, $size);
        $array2 = array_slice($array2, 0, $size);
    }
    return array_combine($array1, $array2);
}

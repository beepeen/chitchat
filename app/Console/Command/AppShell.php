<?php
/**
 * AppShell file
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         CakePHP(tm) v 2.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Shell', 'Console');

/**
 * Application Shell
 *
 * Add your application-wide methods in the class below, your shells
 * will inherit them.
 *
 * @package       app.Console.Command
 */
class AppShell extends Shell {

    /** @var array List of methods to block */
    private $__blockList = array();

    /**
     * Constructor
     */
    public function __construct() {
        parent::__construct();

        $this->__blockList = array(
            'excelImport',
            'fetchAndUpdateExternalUrlLog',
            'importFromExternalUrl',
            'systemSurveyFileGeneration',
            'systemAllSurveyFileGeneration',
            'systemAllCtSurveyFileGeneration',
            'deleteSoftDeletedSurveys',
            'sampleImport',
            'fetchFile'
        );
    }

    public function startUp() {
        if( self::__isDateTimeBetween('23:00', '00:30', date('H:i')) &&
            in_array( $this->command, $this->__blockList) ) {
            $this->out('Cron inactive.');
            CakeLog::write('import', 'Cron inactive, time: ' . date('H:i:s'));
            exit(0);
        }
    }


    /**
     * Check if the time is in between given timestamps
     *
     * @param  string $from  Date string in format hh:mm
     * @param  string $till  Date string in format hh:mm
     * @param  string $input Date string to compare to
     *
     * @return boolean
     */
    private function __isDateTimeBetween($from, $till, $input) {
        $f = DateTime::createFromFormat('!H:i', $from);
        $t = DateTime::createFromFormat('!H:i', $till);
        $i = DateTime::createFromFormat('!H:i', $input);
        if ($f > $t) {
            $t->modify('+1 day');
        }

        return ($f <= $i && $i <= $t) || ($f <= $i->modify('+1 day') && $i <= $t);
    }

}

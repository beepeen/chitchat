# Chitchat

**Team Members**  
Bipin Manandhar, Raju Upadhaya

> Built with CakePHP 2.5.0

## Version Info
**v1.0.0**
    - Basic site setup
    - User login module
    - Search module
    - Chat